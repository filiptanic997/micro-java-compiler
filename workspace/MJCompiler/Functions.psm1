function Compile-Compiler {
    param  (
        [Parameter()][boolean] $IsDebug = $false,
        [Parameter()][boolean] $RegenerateAST = $false
    )

    $callingLocation = Get-Location
    Set-Location "$env:MicroJavaCompilerScriptRoot\src"

    if($RegenerateAST -eq $true){
        Remove-Item .\rs\ac\bg\etf\pp1\ast\*.java -Force
        java -cp ..\lib\JFlex.jar JFlex.Main -d ..\src\rs\ac\bg\etf\pp1 ..\spec\mjlexer.flex
        java -cp ../lib/cup_v10k.jar java_cup.Main -destdir rs\ac\bg\etf\pp1 -parser MJParser -ast rs.ac.bg.etf.pp1.ast -buildtree ..\spec\mjparser.cup
    }

    $javaFiles = Get-ChildItem -Path rs\ac\bg\etf\pp1\util -Filter *.java -Recurse | %{ $_.FullName };
    $javaFiles = $javaFiles + (Get-ChildItem -Path rs\ac\bg\etf\pp1\ast -Filter *.java -Recurse | %{ $_.FullName });
    $javaFiles = $javaFiles + (Get-ChildItem -Path rs\ac\bg\etf\pp1\constants -Filter *.java -Recurse | %{ $_.FullName });
    $javaFiles = $javaFiles + (Get-ChildItem -Path rs\ac\bg\etf\pp1\heuristic -Filter *.java -Recurse | %{ $_.FullName });
    $javaFiles = $javaFiles + (Get-ChildItem -Path rs\ac\bg\etf\pp1\optimizer\ast -Filter *.java | %{ $_.FullName });
    $javaFiles = $javaFiles + (Get-ChildItem -Path rs\ac\bg\etf\pp1\optimizer -Filter *.java | %{ $_.FullName });
    $javaFiles = $javaFiles + (Get-ChildItem -Path rs\ac\bg\etf\pp1 -Filter *.java | %{ $_.FullName });
    Get-ChildItem -Path "..\test" -Filter *.mj | Copy-Item -Destination "..\bin\";

    if($IsDebug -eq $true){
        javac -cp ..\lib\* $javaFiles -d ..\bin -g;
    }
    else{
        javac -cp ..\lib\* $javaFiles -d ..\bin;
    }
    
    Set-Location $callingLocation
}

function Compile-MicroJavaProgram {
    param (
        [Parameter(Mandatory = $true)][string] $SourcePath,
        [ValidateSet("GenerateNonOptimizedCode", "GenerateCallCountCode", "GenerateOptimizedCode")]
        [Parameter()][string] $OptimizationType = "GenerateNonOptimizedCode",
        [string] $DestinationPath = $SourcePath.replace(".mj", ".obj"),
        [Parameter()][int] $MinMethodCallCount = 9999,
        [Parameter()][int] $MaxInlineLevel = 3,
        [Parameter()][float] $MinMethodCallCountToMemoryFootprint = 0.01,
        [Parameter()][int] $LoopUnrollingFactor = 4,
        [Parameter()][int] $LoopUnrollingMinIterationCount = 10000
    )

    $callingLocation = Get-Location
    Set-Location "$env:MicroJavaCompilerScriptRoot\src"

    if($OptimizationType -ieq "GenerateOptimizedCode"){
        Set-Location ..\bin; java -cp "..\lib\*;." rs.ac.bg.etf.pp1.MJCompiler $SourcePath $DestinationPath GenerateCallCountCode | Out-Null;
        $runResults = java -cp ..\lib\mj-runtime.jar rs.etf.pp1.mj.runtime.Run $DestinationPath;
        $callidsFile = "$DestinationPath.callids"
        $callids = Get-Content $callidsFile
        $callids = $callids.Split([System.Environment]::NewLine)
        # Create cnt file
        $runResults = $runResults.Split([System.Environment]::NewLine)
        $runResults = $runResults | Select-Object -First ($runResults.Length - 1)
        $runResults = $runResults.split("n", [System.StringSplitOptions]::RemoveEmptyEntries)
        # Append method call counts
        $methodCallCounts = $runResults[0]
        $methodCallCounts = $methodCallCounts.Split(" ",[System.StringSplitOptions]::RemoveEmptyEntries)
        
        for($i = 0; $i -lt $callids.length; $i++){
            $callids[$i] = $callids[$i] + "," + $methodCallCounts[$i]
        }

        $cntFile = "$DestinationPath.callcnt"
        $callids | Set-Content -Path $cntFile

        # TO-DO: Add logic to merge for loop iteration counts
        $forLoopIterationCounts = $runResults[1]
        $forLoopIterationCounts = $forLoopIterationCounts.Split(" ",[System.StringSplitOptions]::RemoveEmptyEntries)

        $loopCntFile = "$DestinationPath.loopcnt"
        $forLoopIterationCounts | Set-Content -Path $loopCntFile

        # Compile again with optimizations
        Set-Location ..\bin; java -cp "..\lib\*;." rs.ac.bg.etf.pp1.MJCompiler "$SourcePath" "$DestinationPath" GenerateOptimizedCode "$cntFile" "$loopCntFile" $MinMethodCallCount $MaxInlineLevel $MinMethodCallCountToMemoryFootprint $LoopUnrollingFactor $LoopUnrollingMinIterationCount;
    }
    elseif ($OptimizationType -ieq "GenerateCallCountCode"){
        Set-Location ..\bin; java -cp "..\lib\*;." rs.ac.bg.etf.pp1.MJCompiler $SourcePath $DestinationPath GenerateCallCountCode | Out-Null;
    }
    else 
    {
        Set-Location ..\bin; java -cp "..\lib\*;." rs.ac.bg.etf.pp1.MJCompiler $SourcePath $DestinationPath GenerateNonOptimizedCode;
    }

    Set-Location $callingLocation
}

function Disassemble-MicroJavaProgram {
    param (
        [Parameter(Mandatory = $true)][string] $SourcePath
    )

    $callingLocation = Get-Location
    Set-Location "$env:MicroJavaCompilerScriptRoot\src"

    Set-Location ..\bin; java -cp ..\lib\mj-runtime.jar rs.etf.pp1.mj.runtime.disasm $SourcePath;

    Set-Location $callingLocation
}

function Run-MicroJavaProgram {
    param (
        [Parameter(Mandatory = $true)][string] $SourcePath
    )

    $callingLocation = Get-Location
    Set-Location "$env:MicroJavaCompilerScriptRoot\src"

    Set-Location ..\bin; java -cp ..\lib\mj-runtime.jar rs.etf.pp1.mj.runtime.Run $SourcePath;

    Set-Location $callingLocation
}

function Benchmark-MicroJavaProgram {
    param (
        [Parameter(Mandatory = $true)][string] $SourcePath,
        [Parameter(Mandatory = $false)][int] $NumberOfRuns = 20,
        [Parameter(Mandatory = $false)][string] $ResultDumpPath = $SourcePath.replace(".obj", ".obj.results")
    )

    $callingLocation = Get-Location
    Set-Location "$env:MicroJavaCompilerScriptRoot\src"

    $runTimes = [System.Collections.ArrayList]@();
    Set-Location ..\bin;

    for ($i = 0; $i -lt $NumberOfRuns; $i++) {
        $runOutput = java -cp ..\lib\mj-runtime.jar rs.etf.pp1.mj.runtime.Run $SourcePath;
        $runTimes.Add($runOutput.split(" ")[3]);
    }
    
    Write-Host $runTimes;
    $runTimes > $ResultDumpPath

    Set-Location $callingLocation
}

function Validate-Compiler {
    param (
        
    )
	
	$callingLocation = Get-Location
    Set-Location "$env:MicroJavaCompilerScriptRoot\src"

    $runTimes = [System.Collections.ArrayList]@();
    Set-Location ..\bin;

    Compile-Compiler -RegenerateAST $true -IsDebug $true;
	
	foreach ($i in @(1, 2, 3)) { Compile-MicroJavaProgram .\opt_test_x$i.mj GenerateOptimizedCode -LoopUnrollingFactor 4; }
	foreach ($i in @(0, 1, 2, 3, 4, 5, 6)) { Compile-MicroJavaProgram .\opt_test_0$i.mj GenerateOptimizedCode -LoopUnrollingFactor 4; }
	
	Get-ChildItem -Path .\ -Filter *.obj -File -Name | ForEach-Object {
		$compiledProgram = Disassemble-MicroJavaProgram .\$_
		$validationProgram = Disassemble-MicroJavaProgram ..\test\validate\$_
		
		if ($compiledProgram.Length -ne $validationProgram.Length){
			throw "Compiled file length [$($compiledProgram.Length)] is not equal to validation file length [$($validationProgram.Length)]!"
		}
		
		for ($i = 0; $i -lt $compiledProgram.Length; $i++){
			if ($compiledProgram[$i] -ine $validationProgram[$i]){
				throw "Compiled file does not match validation file on line [$i]"
			}
		}
		
		Write-Host "Successfully validated file $_!"
	}
	
	Write-Host "Compiler successfully validated!"
	
	Set-Location $callingLocation
}