package rs.ac.bg.etf.pp1.heuristic;

public interface IFloatParameter {
    float Value();
}
