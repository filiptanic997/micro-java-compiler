package rs.ac.bg.etf.pp1.optimizer.ast;

import rs.ac.bg.etf.pp1.CodeGenerator;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.heuristic.Heuristic;
import rs.ac.bg.etf.pp1.optimizer.InlineOptimizerPass;

public class OptimizerInlinedMethodCallClass extends MethodCallClass {
    
    private FormPars0Or1 formalParameters;
    private ActualPars0Or1 actualParameters;
    private VarDeclaration0OrMany localVariablesDeclarations;
    private Statement0OrMany statements;
    private OptimizerInlinedOpenScope openScope;
    private OptimizerInlinedCloseScope closeScope;

    public OptimizerInlinedMethodCallClass(
        FormPars0Or1 formalParameters,
        ActualPars0Or1 actualParameters,
        VarDeclaration0OrMany localVariablesDeclarations,
        Statement0OrMany statements,
        OptimizerInlinedOpenScope openScope,
        OptimizerInlinedCloseScope closeScope
    )
    {
        super(null, null);
        this.formalParameters = formalParameters;
        this.actualParameters = actualParameters;
        this.localVariablesDeclarations = localVariablesDeclarations;
        this.statements = statements;
        this.openScope = new OptimizerInlinedOpenScope();
        this.closeScope = new OptimizerInlinedCloseScope();
    }
    
    public FormPars0Or1 GetFormalParameters(){
        return this.formalParameters;
    }

    public void SetFormalParameters(FormPars0Or1 formalparameters){
        this.formalParameters = formalparameters;
    }

    public ActualPars0Or1 GetActualParameters(){
        return this.actualParameters;
    }

    public void SetActualParameters(ActualPars0Or1 actualParameters){
        this.actualParameters = actualParameters;
    }

    public VarDeclaration0OrMany GetLocalVariablesDeclarations(){
        return this.localVariablesDeclarations;
    }

    public void SetLocalVariablesDeclarations(VarDeclaration0OrMany localVariablesDeclarations){
        this.localVariablesDeclarations = localVariablesDeclarations;
    }

    public Statement0OrMany GetStatements(){
        return this.statements;
    }

    public void SetStatements(Statement0OrMany statements){
        this.statements = statements;
    }

    public void accept(Visitor visitor) {
        if(visitor instanceof CodeGenerator){
            ((CodeGenerator)visitor).visit(this);
        }
        else if(visitor instanceof InlineOptimizerPass){
            ((InlineOptimizerPass)visitor).visit(this);
        }
        else if(visitor instanceof SemanticAnalyzer){
            ((SemanticAnalyzer)visitor).visit(this);
        }
    }

    public void childrenAccept(Visitor visitor) {
        openScope.accept(visitor);
        if(formalParameters != null) formalParameters.accept(visitor);
        if(actualParameters != null) actualParameters.accept(visitor);
        if(localVariablesDeclarations != null) localVariablesDeclarations.accept(visitor);
        if(statements != null) statements.accept(visitor);
        closeScope.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(openScope!=null) openScope.traverseTopDown(visitor);
        if(formalParameters != null) formalParameters.traverseTopDown(visitor);
        if(actualParameters != null) actualParameters.traverseTopDown(visitor);
        if(localVariablesDeclarations != null) localVariablesDeclarations.traverseTopDown(visitor);
        if(statements != null) statements.traverseTopDown(visitor);
        if(closeScope!=null) closeScope.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(openScope!=null) openScope.traverseBottomUp(visitor);
        if(formalParameters != null) formalParameters.traverseBottomUp(visitor);
        if(actualParameters != null) actualParameters.traverseBottomUp(visitor);
        if(localVariablesDeclarations != null) localVariablesDeclarations.traverseBottomUp(visitor);
        if(statements != null) statements.traverseBottomUp(visitor);
        if(closeScope!=null) closeScope.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("OptimizerInlinedFactorDesignatorMethodCallClass(\n");

        /*if(formalParameters!=null)
            buffer.append(formalParameters.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");*/

        if(actualParameters!=null)
            buffer.append(actualParameters.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        /*if(localVariablesDeclarations!=null)
            buffer.append(localVariablesDeclarations.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");*/

        if(statements!=null)
            buffer.append(statements.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [OptimizerInlinedFactorDesignatorMethodCallClass]");
        return buffer.toString();
    }
}
