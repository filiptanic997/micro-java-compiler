package rs.ac.bg.etf.pp1.optimizer.ast;

import rs.ac.bg.etf.pp1.CodeGenerator;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.optimizer.InlineOptimizerPass;

public class OptimizerInlinedCloseScope implements SyntaxNode {

    @Override
    public void accept(Visitor visitor) {
        if(visitor instanceof CodeGenerator){
            ((CodeGenerator)visitor).visit(this);
        }
        else if(visitor instanceof InlineOptimizerPass){
            ((InlineOptimizerPass)visitor).visit(this);
        }
        else if(visitor instanceof SemanticAnalyzer){
            ((SemanticAnalyzer)visitor).visit(this);
        }
    }

    @Override
    public void childrenAccept(Visitor visitor) {
        
    }

    @Override
    public void traverseBottomUp(Visitor visitor) {
        accept(visitor);
    }

    @Override
    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
    }

    @Override
    public SyntaxNode getParent() {
        return null;
    }

    @Override
    public void setParent(SyntaxNode parent) {
        
    }

    @Override
    public int getLine() {
        return -1;
    }

    @Override
    public void setLine(int line) {
        
    }
    
}
