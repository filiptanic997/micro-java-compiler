package rs.ac.bg.etf.pp1.heuristic;

import java.util.Map;

public class SingleMethodCallCountToMemoryFootprintHeuristic implements IMethodHeuristic {

    private Map<String, Integer> methodCallCounts;
	private Map<String, Integer> methodMemoryFootprints;

    public SingleMethodCallCountToMemoryFootprintHeuristic(
        Map<String, Integer> methodCallCounts,
        Map<String, Integer> methodMemoryFootprints)
    {
        this.methodCallCounts = methodCallCounts;
        this.methodMemoryFootprints = methodMemoryFootprints;
    }

    @Override
    public boolean ShouldApply(String methodName) {
        // TO-DO: Implement a better design?
        return ShouldApply(methodName, 0);
    }

    @Override
    public boolean ShouldApply(String methodName, int value) {
        return ((float)methodCallCounts.get(methodName)) / methodMemoryFootprints.get(methodName) >= value;
    }
    
    @Override
    public boolean ShouldApply(String methodName, float value) {
        return ((float)methodCallCounts.get(methodName)) / methodMemoryFootprints.get(methodName) >= value;
    }
}
