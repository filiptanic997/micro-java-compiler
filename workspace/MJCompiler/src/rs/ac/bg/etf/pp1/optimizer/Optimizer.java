package rs.ac.bg.etf.pp1.optimizer;

import java.util.ArrayList;
import java.util.List;

import rs.ac.bg.etf.pp1.ast.SyntaxNode;
import rs.ac.bg.etf.pp1.constants.Constants.OptimizationType;

public class Optimizer {
    
    private static Optimizer optimizer = new Optimizer();
    private OptimizationType optimizationType = OptimizationType.GenerateOptimizedCode;
    private List<IOptimizerPass> optimizerPasses = new ArrayList<>();

    private Optimizer(){

    }

    public static Optimizer Instance(){
        return optimizer;
    }

    public OptimizationType GetOptimizationType(){
        return optimizationType;
    }

    public void SetOptimizationType(OptimizationType optimizationType){
        this.optimizationType = optimizationType;
    }

    public List<IOptimizerPass> GetOptimizerPasses(){
        return optimizerPasses;
    }

    public void AddOptimizerPass(IOptimizerPass optimizerPass){
        optimizerPasses.add(optimizerPass);
    }

    public void PerformOptimizationPasses(SyntaxNode abstractSyntaxTree){
        for (IOptimizerPass optimizerPass : optimizerPasses) {
            optimizerPass.PerformPass(abstractSyntaxTree);
        }
    }

}
