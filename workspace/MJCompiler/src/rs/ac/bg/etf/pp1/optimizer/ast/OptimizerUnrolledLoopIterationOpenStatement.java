package rs.ac.bg.etf.pp1.optimizer.ast;

import rs.ac.bg.etf.pp1.*;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.optimizer.*;

public class OptimizerUnrolledLoopIterationOpenStatement extends OptimizerUnrolledLoopIteration {

    private StatementOpen statements;

    public OptimizerUnrolledLoopIterationOpenStatement(SyntaxNode parent,
        OptimizerUnrolledLoopIteration previousIteration,
        OptimizerUnrolledBeginIteration beginIteration,
        OptimizerUnrolledEndIteration endIteration,
        DesignatorStatement0Or1 update,
        StatementOpen statements)
    {
        super(previousIteration, beginIteration, endIteration, update);

        this.statements = statements;
        if(statements != null) statements.setParent(this);
    }

    @Override
    public void accept(Visitor visitor) {
        // No need to visit this node
    }

    @Override
    public void childrenAccept(Visitor visitor) {
        if(beginIteration != null) beginIteration.accept(visitor);
        if(previousIteration != null) previousIteration.accept(visitor);
        if(statements != null) statements.accept(visitor);
        if(endIteration != null) endIteration.accept(visitor);
        if(update != null) update.accept(visitor);
    }

    @Override
    public void traverseBottomUp(Visitor visitor) {
        if(previousIteration != null) previousIteration.traverseBottomUp(visitor);
        if(beginIteration != null) beginIteration.traverseBottomUp(visitor);
        if(statements != null) statements.traverseBottomUp(visitor);
        if(endIteration != null) endIteration.traverseBottomUp(visitor);
        if(update != null) update.traverseBottomUp(visitor);
        accept(visitor);
    }

    @Override
    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(previousIteration != null) previousIteration.traverseTopDown(visitor);
        if(beginIteration != null) beginIteration.traverseTopDown(visitor);
        if(statements != null) statements.traverseTopDown(visitor);
        if(endIteration != null) endIteration.traverseTopDown(visitor);
        if(update != null) update.traverseTopDown(visitor);
    }

    @Override
    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("OptimizerUnrolledLoopIterationOpenStatement(\n");

        if(previousIteration!=null)
            buffer.append(previousIteration.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(beginIteration!=null)
            buffer.append(beginIteration.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(statements!=null)
            buffer.append(statements.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(endIteration!=null)
            buffer.append(endIteration.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(update!=null)
            buffer.append(update.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");        

        buffer.append(tab);
        buffer.append(") [OptimizerUnrolledLoopIterationOpenStatement]");
        return buffer.toString();
    }
    
}
