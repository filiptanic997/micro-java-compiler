package rs.ac.bg.etf.pp1.heuristic;

public class LoopUnrollingMinIterationCountHeuristic implements IIntegerParameter {

    private int minIterationcount;

    public LoopUnrollingMinIterationCountHeuristic(int minIterationcount){
        this.minIterationcount = minIterationcount;
    }

    @Override
    public int Value() {
        return minIterationcount;
    }
}