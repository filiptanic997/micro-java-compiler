package rs.ac.bg.etf.pp1.heuristic;

public interface IMethodHeuristic {
    boolean ShouldApply(String methodName);
    boolean ShouldApply(String methodName, int value);
    boolean ShouldApply(String methodName, float value);
}
