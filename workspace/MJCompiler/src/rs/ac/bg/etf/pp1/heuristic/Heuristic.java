package rs.ac.bg.etf.pp1.heuristic;

import java.util.ArrayList;
import java.util.List;

public class Heuristic {
    
    private static List<IGlobalHeuristic> globalHeuristics = new ArrayList<>();
    private static List<IMethodHeuristic> methodHeuristics = new ArrayList<>();
    private static List<IFloatParameter> floatHeuristics = new ArrayList<>();
    private static List<IIntegerParameter> integerHeuristics = new ArrayList<>();

    public static void AddGlobalHeuristic(IGlobalHeuristic globalHeuristic){
        globalHeuristics.add(globalHeuristic);
    }

    public static List<IGlobalHeuristic> GetGlobalHeuristics(){
        return globalHeuristics;
    }

    public static void AddMethodHeuristic(IMethodHeuristic methodHeuristic){
        methodHeuristics.add(methodHeuristic);
    }

    public static List<IMethodHeuristic> GetMethodHeuristics(){
        return methodHeuristics;
    }

    public static void AddFloatHeuristic(IFloatParameter floatHeuristic){
        floatHeuristics.add(floatHeuristic);
    }

    public static List<IFloatParameter> GetFloatHeuristics(){
        return floatHeuristics;
    }

    public static void AddIntegerHeuristic(IIntegerParameter integerHeuristic){
        integerHeuristics.add(integerHeuristic);
    }

    public static List<IIntegerParameter> GetIntegerHeuristics(){
        return integerHeuristics;
    }

    public static boolean ShouldInlineMethodOnDynamicCallCount(String methodName, int methodCallCount){
        boolean returnValue = true;

        for (IGlobalHeuristic globalHeuristic : globalHeuristics) {
            if(globalHeuristic instanceof MethodMinDynamicCallCountInlineHeuristic){
                returnValue = globalHeuristic.ShouldApply(methodCallCount);

                break;
            }
        }

        for (IMethodHeuristic methodHeuristic : methodHeuristics) {
            if(methodHeuristic instanceof SingleMethodMinDynamicCallCountInlineHeuristic){
                returnValue = methodHeuristic.ShouldApply(methodName, methodCallCount);

                break;
            }
        }

        return returnValue;
    }

    public static boolean ShouldInlineMethodOnCurrentInlineLevel(int currentInlineLevel){
        boolean returnValue = true;

        for (IGlobalHeuristic globalHeuristic : globalHeuristics) {
            if(globalHeuristic instanceof MethodMaxInlineLevelHeuristic){
                returnValue = globalHeuristic.ShouldApply(currentInlineLevel);

                break;
            }
        }

        return returnValue;
    }

    public static boolean ShouldInlineMethodOnCallCountToMemoryFootprintRatio(String methodName){
        boolean returnValue = false;
        float methodMinCallCountToMemoryFootprintRatio = -1f;

        for (IFloatParameter floatHeuristic : floatHeuristics) {
            if(floatHeuristic instanceof MethodMinCallCountToMemoryFootprintHeuristic){
                methodMinCallCountToMemoryFootprintRatio = floatHeuristic.Value();

                break;
            }
        }

        for (IMethodHeuristic methodHeuristic : methodHeuristics) {
            if(methodHeuristic instanceof SingleMethodCallCountToMemoryFootprintHeuristic){
                returnValue = methodHeuristic.ShouldApply(methodName, methodMinCallCountToMemoryFootprintRatio);

                break;
            }
        }

        return returnValue;
    }

    public static boolean ShouldUnrollLoopOnIterationCount(int loopIterationCount){
        for (IIntegerParameter integerHeuristic : integerHeuristics) {
            if(integerHeuristic instanceof LoopUnrollingMinIterationCountHeuristic){
                return loopIterationCount >= integerHeuristic.Value();
            }
        }

        return false;
    }

    public static int UnrollFactor(){
        for (IIntegerParameter integerHeuristic : integerHeuristics) {
            if(integerHeuristic instanceof LoopUnrollingLevelHeuristic){
                return integerHeuristic.Value();
            }
        }

        return 1;
    }

}