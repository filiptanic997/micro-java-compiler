package rs.ac.bg.etf.pp1.heuristic;

public class LoopUnrollingLevelHeuristic implements IIntegerParameter {

    private int loopUnrollinglevel;

    public LoopUnrollingLevelHeuristic(int loopUnrollinglevel){
        this.loopUnrollinglevel = loopUnrollinglevel;
    }

    @Override
    public int Value() {
        return loopUnrollinglevel;
    }
}
