package rs.ac.bg.etf.pp1.heuristic;

// Heuristic not related to any method
public interface IGlobalHeuristic {
    boolean ShouldApply();
    boolean ShouldApply(int value);
    boolean ShouldApply(float value);
}
