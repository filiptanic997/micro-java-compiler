package rs.ac.bg.etf.pp1.heuristic;

public class MethodMinCallCountToMemoryFootprintHeuristic implements IFloatParameter {
    
    private float methodMinCallCountToMemoryFootprintRatio;

    public MethodMinCallCountToMemoryFootprintHeuristic(float methodMinCallCountToMemoryFootprintRatio){
        this.methodMinCallCountToMemoryFootprintRatio = methodMinCallCountToMemoryFootprintRatio;
    }

    @Override
    public float Value() {
        return methodMinCallCountToMemoryFootprintRatio;
    }

}
