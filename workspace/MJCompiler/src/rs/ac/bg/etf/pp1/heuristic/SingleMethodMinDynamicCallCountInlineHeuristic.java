package rs.ac.bg.etf.pp1.heuristic;

public class SingleMethodMinDynamicCallCountInlineHeuristic implements IMethodHeuristic {

    private String methodName;
    private int methodDynamicCallCount;

    public SingleMethodMinDynamicCallCountInlineHeuristic(String methodName, int methodDynamicCallCount){
        this.methodName = methodName;
        this.methodDynamicCallCount = methodDynamicCallCount;
    }

    @Override
    public boolean ShouldApply(String methodName) {
        // TO-DO: Implement a better design?
        return ShouldApply(methodName, 0);
    }

    @Override
    public boolean ShouldApply(String methodName, int value) {
        return this.methodName.equals(methodName) &&
                value >= methodDynamicCallCount;
    }
    
    @Override
    public boolean ShouldApply(String methodName, float value) {
        return this.methodName.equals(methodName) &&
                value >= methodDynamicCallCount;
    }
}