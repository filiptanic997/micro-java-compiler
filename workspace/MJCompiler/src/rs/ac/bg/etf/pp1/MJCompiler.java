package rs.ac.bg.etf.pp1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java_cup.runtime.Symbol;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import rs.ac.bg.etf.pp1.ast.Program;
import rs.ac.bg.etf.pp1.constants.Constants.OptimizationType;
import rs.ac.bg.etf.pp1.heuristic.Heuristic;
import rs.ac.bg.etf.pp1.heuristic.LoopUnrollingLevelHeuristic;
import rs.ac.bg.etf.pp1.heuristic.LoopUnrollingMinIterationCountHeuristic;
import rs.ac.bg.etf.pp1.heuristic.MethodMaxInlineLevelHeuristic;
import rs.ac.bg.etf.pp1.heuristic.MethodMinCallCountToMemoryFootprintHeuristic;
import rs.ac.bg.etf.pp1.heuristic.MethodMinDynamicCallCountInlineHeuristic;
import rs.ac.bg.etf.pp1.heuristic.SingleMethodCallCountToMemoryFootprintHeuristic;
import rs.ac.bg.etf.pp1.optimizer.*;
import rs.ac.bg.etf.pp1.util.Log4JUtils;
//import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.mj.runtime.Code;

public class MJCompiler {

	static {
		DOMConfigurator.configure(Log4JUtils.instance().findLoggerConfigFile());
		Log4JUtils.instance().prepareLogFile(Logger.getRootLogger());
	}

	private static OptimizationType optimizationType = OptimizationType.GenerateNonOptimizedCode;
	private static int minMethodDynamicCallCount = 7;
	private static int maxInlineLevel = 3;

	private static Map<String, Integer> methodCallCounts;
	private static Map<String, Integer> methodMemoryFootprints;
	private static List<Integer> loopIterationCounts;
	
	public static void main(String[] args) throws Exception {
		
		Logger log = Logger.getLogger(MJCompiler.class);
		
		Reader reader = null;
		try {
			String sourceFilePath = args[0];
			String targetFilePath = args[1];
			if(args.length > 2){
				optimizationType = OptimizationType.valueOf(args[2]);
			}

			// Front end
			log.info("Compiling source file: " + sourceFilePath);
			
			reader = new BufferedReader(new FileReader(sourceFilePath));
			Yylex lexer = new Yylex(reader);
			
			MJParser p = new MJParser(lexer);
	        Symbol s = p.parse();  //pocetak parsiranja
	        
	        Program prog = (Program)(s.value); 
			// ispis sintaksnog stabla
			log.info(prog.toString(""));
			log.info("===================================");

			// ispis prepoznatih programskih konstrukcija
			//RuleVisitor v = new RuleVisitor();
			SemanticAnalyzer sA = new SemanticAnalyzer();
			prog.traverseBottomUp(sA);
			
			log.info("===================================");
			Tab.dump();
			
			if(!p.errorDetected && sA.passed()){
				File objFile = new File(targetFilePath);
				if(objFile.exists()) {
					objFile.delete();
				}

				if(optimizationType == OptimizationType.GenerateOptimizedCode){
					String sourceCallCountsFilePath = args[3];
					String sourceLoopCountsFilePath = args[4];
					minMethodDynamicCallCount = Integer.parseInt(args[5]);
					maxInlineLevel = Integer.parseInt(args[6]);					
					BufferedReader br = null;

					log.info("Reading method call counts from file: " + sourceCallCountsFilePath);

					try{
						br = new BufferedReader(new FileReader(sourceCallCountsFilePath));
						methodCallCounts = new HashMap<>();
						methodMemoryFootprints = new HashMap<>();
						ReadMethodCallMetrics(br);
					}
					catch (FileNotFoundException ex){ }
					finally{
						if(br != null){
							br.close();
						}
					}

					log.info("Reading loop iteration counts from file: " + sourceLoopCountsFilePath);

					try{
						br = new BufferedReader(new FileReader(sourceLoopCountsFilePath));
						loopIterationCounts = new ArrayList<>();
						ReadLoopIterationMetrics(br);
					}
					catch (FileNotFoundException ex){ }
					finally{
						if(br != null){
							br.close();
						}
					}

					Heuristic.AddFloatHeuristic(new MethodMinCallCountToMemoryFootprintHeuristic(Float.parseFloat(args[7])));
					Heuristic.AddMethodHeuristic(new SingleMethodCallCountToMemoryFootprintHeuristic(methodCallCounts, methodMemoryFootprints));
					Heuristic.AddIntegerHeuristic(new LoopUnrollingLevelHeuristic(Integer.parseInt(args[8])));
					Heuristic.AddIntegerHeuristic(new LoopUnrollingMinIterationCountHeuristic(Integer.parseInt(args[9])));
					
					Optimizer.Instance().AddOptimizerPass(new LoopUnrollingOptimizerPass(optimizationType, loopIterationCounts));
					Optimizer.Instance().AddOptimizerPass(new InlineOptimizerPass(optimizationType, methodCallCounts));
				}
				/*else{
					Optimizer.Instance().AddOptimizerPass(new LoopUnrollingOptimizerPass(OptimizationType.GenerateNonOptimizedCode, null));
					Optimizer.Instance().AddOptimizerPass(new InlineOptimizerPass(OptimizationType.GenerateNonOptimizedCode, null));
				}*/

				// Insert global heuristics, will be overriden above if GenerateOptimizedCode is specified
				Heuristic.AddGlobalHeuristic(new MethodMinDynamicCallCountInlineHeuristic(minMethodDynamicCallCount));
				Heuristic.AddGlobalHeuristic(new MethodMaxInlineLevelHeuristic(maxInlineLevel));

				// Optimizer
				Optimizer.Instance().PerformOptimizationPasses(prog);
				
				// Reusing SemanticAnalyzer to repopulate optimized AST with symbols
				sA = new SemanticAnalyzer();
				prog.traverseBottomUp(sA);

				// DEBUG
				// ispis sintaksnog stabla
				log.info(prog.toString(""));

				// Back end
				CodeGenerator codeGenerator = new CodeGenerator(optimizationType, sA.globalVarCounter, sA.globalMethodDeclarationCount, sA.forLoopDeclarationCount);
				prog.traverseBottomUp(codeGenerator);
				Code.dataSize = sA.getGlobalVarCount() + 
					(optimizationType == OptimizationType.GenerateCallCountCode ?
					 sA.globalMethodDeclarationCount + sA.forLoopDeclarationCount :
					 0);
				Code.mainPc = codeGenerator.getMainPc();
				Code.write(new FileOutputStream(objFile));
				log.info("Parsiranje uspesno zavrseno!");
				
				if(optimizationType == OptimizationType.GenerateCallCountCode){
					File methodCallIdsFile = new File(targetFilePath + ".callids");
					if(methodCallIdsFile.exists()) {
						methodCallIdsFile.delete();
					}

					Map<String, Integer> methodMemoryFootprints = codeGenerator.GetMethodMemoryFootprints();

					try(FileWriter fw = new FileWriter(methodCallIdsFile)){
						for (String methodCallId : codeGenerator.GetMethodCallIds().keySet()) {
							fw.write(methodCallId + "," + methodMemoryFootprints.get(methodCallId) + "\n");
						}
					}
				}
			}
			else {
				log.error("Parsiranje NIJE uspesno zavrseno!");
			}
			
		} 
		finally {
			if (reader != null) try { reader.close(); } catch (IOException e1) { log.error(e1.getMessage(), e1); }
		}

	}
	
	public static void ReadMethodCallMetrics(BufferedReader br) throws NumberFormatException, IOException{
		String line;

		while((line = br.readLine()) != null){
			String[] methodCallCount = line.split(",");
			methodMemoryFootprints.put(methodCallCount[0], Integer.parseInt(methodCallCount[1]));
			methodCallCounts.put(methodCallCount[0], Integer.parseInt(methodCallCount[2]));
		}
	}

	public static void ReadLoopIterationMetrics(BufferedReader br) throws NumberFormatException, IOException{
		String line;

		while((line = br.readLine()) != null){
			loopIterationCounts.add(Integer.parseInt(line));
		}
	}

}
