package rs.ac.bg.etf.pp1.optimizer.ast;

import rs.ac.bg.etf.pp1.ast.*;

public abstract class OptimizerUnrolledLoopIteration implements SyntaxNode {

    protected SyntaxNode parent;
    protected OptimizerUnrolledLoopIteration previousIteration;
    protected OptimizerUnrolledBeginIteration beginIteration;
    protected OptimizerUnrolledEndIteration endIteration;
    protected DesignatorStatement0Or1 update;

    public OptimizerUnrolledLoopIteration(SyntaxNode parent,
        OptimizerUnrolledLoopIteration previousIteration,
        OptimizerUnrolledBeginIteration beginIteration,
        OptimizerUnrolledEndIteration endIteration,
        DesignatorStatement0Or1 update)
    {
        this(previousIteration, beginIteration, endIteration, update);
    }
    
    public OptimizerUnrolledLoopIteration(OptimizerUnrolledLoopIteration previousIteration,
        OptimizerUnrolledBeginIteration beginIteration,
        OptimizerUnrolledEndIteration endIteration,
        DesignatorStatement0Or1 update)
    {
        this.previousIteration = previousIteration;
        if(previousIteration != null) previousIteration.setParent(this);
        this.beginIteration = beginIteration;
        if(beginIteration != null) beginIteration.setParent(this);
        this.endIteration = endIteration;
        if(endIteration != null) endIteration.setParent(this);
        this.update = update;
        if(update != null) update.setParent(this);
    }

    @Override
    public SyntaxNode getParent() {
        return this.parent;
    }

    @Override
    public void setParent(SyntaxNode parent) {
        this.parent = parent;
    }

    @Override
    public int getLine() {
        return -1;
    }

    @Override
    public void setLine(int line) {       
        
    }

    public String toString(String tab){
        return toString("");
    }
    
}
