package rs.ac.bg.etf.pp1.optimizer.ast;

import rs.ac.bg.etf.pp1.*;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.optimizer.*;

public class OptimizerUnrolledBeginCondition implements SyntaxNode {

    private SyntaxNode parent;

    public OptimizerUnrolledBeginCondition(SyntaxNode parent){
        
    }
    
    public OptimizerUnrolledBeginCondition(){

    }    

    @Override
    public void accept(Visitor visitor) {
        if(visitor instanceof CodeGenerator){
            ((CodeGenerator)visitor).visit(this);
        }
    }

    @Override
    public void childrenAccept(Visitor visitor) {
        accept(visitor);
    }

    @Override
    public void traverseBottomUp(Visitor visitor) {
        accept(visitor);
    }

    @Override
    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
    }

    @Override
    public SyntaxNode getParent() {
        return this.parent;
    }

    @Override
    public void setParent(SyntaxNode parent) {
        this.parent = parent;
        
    }

    @Override
    public int getLine() {        
        return -1;
    }

    @Override
    public void setLine(int line) {

    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("OptimizerUnrolledBeginCondition(\n");       

        buffer.append(tab);
        buffer.append(") [OptimizerUnrolledBeginCondition]");
        return buffer.toString();
    }
    
}
