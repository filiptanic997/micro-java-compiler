package rs.ac.bg.etf.pp1.optimizer.ast;

import rs.ac.bg.etf.pp1.*;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.optimizer.*;

public class OptimizerUnrolledLoopClosedFor extends StatementClosedForClass {

    private OptimizerUnrolledBeginFor beginFor;
    private DesignatorStatement0Or1 initialize;
    private OptimizerUnrolledLoopIteration peeledOffIterations;
    private OptimizerUnrolledBeginCondition beginCondition;
    private Condition0Or1 condition;
    private OptimizerUnrolledLoopIteration unrolledIterations;
    private OptimizerUnrolledEndFor endFor;
    
    public OptimizerUnrolledLoopClosedFor(OptimizerUnrolledBeginFor beginFor,
        DesignatorStatement0Or1 initialize,
        OptimizerUnrolledLoopIteration peeledOffIterations,
        OptimizerUnrolledBeginCondition beginCondition,
        Condition0Or1 condition,
        OptimizerUnrolledLoopIteration unrolledIterations,
        OptimizerUnrolledEndFor endFor)
    {
        super(null, null, null, null, null, null, null, null, null);

        this.beginFor = beginFor;
        if(beginFor != null) beginFor.setParent(this);
        this.initialize = initialize;
        if(initialize != null) initialize.setParent(this);
        this.peeledOffIterations = peeledOffIterations;
        if(peeledOffIterations != null) peeledOffIterations.setParent(this);
        this.beginCondition = beginCondition;
        if(beginCondition != null) beginCondition.setParent(this);
        this.condition = condition;
        if(condition != null) condition.setParent(this);
        this.unrolledIterations = unrolledIterations;
        if(unrolledIterations != null) unrolledIterations.setParent(this);
        this.endFor = endFor;
        if(endFor != null) endFor.setParent(this);
    }
    
    @Override
    public void accept(Visitor visitor) {
        if(visitor instanceof CodeGenerator){
            ((CodeGenerator)visitor).visit(this);
        }
        else if(visitor instanceof LoopUnrollingOptimizerPass){
            ((LoopUnrollingOptimizerPass)visitor).visit(this);
        }
    }

    @Override
    public void childrenAccept(Visitor visitor) {
        if(beginFor != null) beginFor.accept(visitor);
        if(initialize != null) initialize.accept(visitor);
        if(peeledOffIterations != null) peeledOffIterations.accept(visitor);
        if(beginCondition != null) beginCondition.accept(visitor);
        if(condition != null) condition.accept(visitor);
        if(unrolledIterations != null) unrolledIterations.accept(visitor);
        if(endFor != null) endFor.accept(visitor);        
    }

    @Override
    public void traverseBottomUp(Visitor visitor) {
        if(beginFor != null) beginFor.traverseBottomUp(visitor);
        if(initialize != null) initialize.traverseBottomUp(visitor);
        if(peeledOffIterations != null) peeledOffIterations.traverseBottomUp(visitor);
        if(beginCondition != null) beginCondition.traverseBottomUp(visitor);
        if(condition != null) condition.traverseBottomUp(visitor);
        if(unrolledIterations != null) unrolledIterations.traverseBottomUp(visitor);
        if(endFor != null) endFor.traverseBottomUp(visitor);
        accept(visitor);
    }

    @Override
    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(beginFor != null) beginFor.traverseTopDown(visitor);
        if(initialize != null) initialize.traverseTopDown(visitor);
        if(peeledOffIterations != null) peeledOffIterations.traverseTopDown(visitor);
        if(beginCondition != null) beginCondition.traverseTopDown(visitor);
        if(condition != null) condition.traverseTopDown(visitor);
        if(unrolledIterations != null) unrolledIterations.traverseTopDown(visitor);
        if(endFor != null) endFor.traverseTopDown(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("OptimizerUnrolledLoopClosedFor(\n");

        if(beginFor!=null)
            buffer.append(beginFor.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(initialize!=null)
            buffer.append(initialize.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(peeledOffIterations!=null)
            buffer.append(peeledOffIterations.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(beginCondition!=null)
            buffer.append(beginCondition.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(condition!=null)
            buffer.append(condition.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");
        
        if(unrolledIterations!=null)
            buffer.append(unrolledIterations.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n"); 

        if(endFor!=null)
            buffer.append(endFor.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n"); 

        buffer.append(tab);
        buffer.append(") [OptimizerUnrolledLoopClosedFor]");
        return buffer.toString();
    }
    
    // Not used
    private OptimizerUnrolledLoopClosedFor(rs.ac.bg.etf.pp1.ast.OptimizedFor OptimizedFor,
            rs.ac.bg.etf.pp1.ast.DesignatorStatement0Or1 DesignatorStatement0Or1,
            rs.ac.bg.etf.pp1.ast.BeginConditionFor BeginConditionFor, rs.ac.bg.etf.pp1.ast.Condition0Or1 Condition0Or1,
            rs.ac.bg.etf.pp1.ast.BeginUpdateFor BeginUpdateFor,
            rs.ac.bg.etf.pp1.ast.DesignatorStatement0Or1 DesignatorStatement0Or11,
            rs.ac.bg.etf.pp1.ast.EndUpdateFor EndUpdateFor, rs.ac.bg.etf.pp1.ast.StatementClosed StatementClosed,
            rs.ac.bg.etf.pp1.ast.EndFor EndFor) throws Exception {
        super(OptimizedFor, DesignatorStatement0Or1, BeginConditionFor, Condition0Or1, BeginUpdateFor, DesignatorStatement0Or11,
                EndUpdateFor, StatementClosed, EndFor);
        throw new Exception("This constructor is not implemented");
    }

}