package rs.ac.bg.etf.pp1.heuristic;

public class MethodMinDynamicCallCountInlineHeuristic implements IGlobalHeuristic {

    private int methodDynamicCallCount;

    public MethodMinDynamicCallCountInlineHeuristic(int methodDynamicCallCount){
        this.methodDynamicCallCount = methodDynamicCallCount;
    }

    @Override
    public boolean ShouldApply() {
        // TO-DO: Implement a better design?
        return ShouldApply( 0);
    }

    @Override
    public boolean ShouldApply(int value) {
        return value >= methodDynamicCallCount;
    }

    @Override
    public boolean ShouldApply(float value) {
        return value >= methodDynamicCallCount;
    }
    
}