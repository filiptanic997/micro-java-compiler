package rs.ac.bg.etf.pp1.heuristic;

public class MethodMaxInlineLevelHeuristic implements IGlobalHeuristic {
    
    private int maxInlineLevel;

    public MethodMaxInlineLevelHeuristic(int maxInlineLevel){
        this.maxInlineLevel = maxInlineLevel;
    }

    @Override
    public boolean ShouldApply() {
        // TO-DO: Implement a better design?
        return ShouldApply( 0);
    }

    @Override
    public boolean ShouldApply(int value) {
        // Zero level will be permitted by default, if not specified explicitly
        return value <= maxInlineLevel;
    }

    @Override
    public boolean ShouldApply(float value) {
        // Zero level will be permitted by default, if not specified explicitly
        return value <= maxInlineLevel;
    }

}
