package rs.ac.bg.etf.pp1.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

import rs.ac.bg.etf.pp1.Tab;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.optimizer.ast.*;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.concepts.*;

public class Utilities {
    
    public static boolean AssignValueToPrivateField(SyntaxNode targetNode, SyntaxNode sourceValue, String fieldName){
        boolean success = false;

        try {
            Field targetField = targetNode.getClass().getDeclaredField(fieldName);
            targetField.setAccessible(true);
            targetField.set(targetNode, sourceValue);

            success = true;
        }
        catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        catch (SecurityException e) {
            e.printStackTrace();
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        };

        return success;
    }

    public static MethodName CopyMethodName(MethodName methodName){
        MethodName newMethodName;

        if(methodName instanceof MethodNameVoidClass){
            String methodNameString = ((MethodNameVoidClass)methodName).getMethodName();
            newMethodName = new MethodNameVoidClass(methodNameString);
        }
        else{ // MethodNameNonVoidClass
            Type type = ((MethodNameNonVoidClass)methodName).getType();
            String methodNameString = ((MethodNameNonVoidClass)methodName).getMethodName();
            newMethodName = new MethodNameNonVoidClass(type, methodNameString);
        }

        newMethodName.obj = methodName.obj;

        return newMethodName;
    }

    public static MethodDeclarationClass SemiDeepCopyMethodDeclaration(MethodDeclarationClass methodDeclaration){
        MethodName newMethodName = CopyMethodName(methodDeclaration.getMethodName());
        newMethodName.setParent(methodDeclaration);

        return new MethodDeclarationClass(
            methodDeclaration.getInlineMethod(),
            newMethodName,
            methodDeclaration.getFormPars0Or1(),
            methodDeclaration.getVarDeclaration0OrMany(),
            methodDeclaration.getStatement0OrMany());
    }

    public static boolean IsMethodInlined(MethodDeclaration methodDeclaration){
		SyntaxNode parentNode = methodDeclaration.getParent();

		return parentNode instanceof OptimizerInlinedMethodCallClass;
	}

    public static boolean IsMethodInlined(MethodName methodName){
        SyntaxNode grandParentNode = methodName.getParent().getParent();

		return grandParentNode instanceof OptimizerInlinedMethodCallClass;
    }

    public static int FindCurentMethodMemoryFootprint(){
        int lastEnterPosition = Code.pc;

        for(; lastEnterPosition >= 0; lastEnterPosition--){
            if(Code.buf[lastEnterPosition] == Code.enter){
                break;
            }
        }

        // Signaling some error occured
        if(lastEnterPosition == 0){
            return -1;
        }
        
        return Code.pc - lastEnterPosition;
    }

    public static SyntaxNode DeepCopySubtreeRefl(SyntaxNode root){        
        try {
            if(root == null){
                return null;
            }

            Field[] inheritedProtectedChildNodes = root.getClass().getSuperclass().getDeclaredFields();
            Field[] childNodes = root.getClass().getDeclaredFields();
            ArrayList<Field> allChildNodes = new ArrayList<>();

            for (Field inheritedProtectedChildNode : inheritedProtectedChildNodes) {
                if(Modifier.isProtected(inheritedProtectedChildNode.getModifiers())){
                    allChildNodes.add(inheritedProtectedChildNode);
                }
            }

            for (Field childNode : childNodes) {
                allChildNodes.add(childNode);
            }            

            Object[] ctorParameters = new Object[allChildNodes.size()];

            for(int i = 0; i < allChildNodes.size(); i++) {
                allChildNodes.get(i).setAccessible(true);

                if(allChildNodes.get(i).getType().isPrimitive()
                    || allChildNodes.get(i).getType().equals(String.class)
                    || allChildNodes.get(i).getType().getSuperclass() != null &&
                       allChildNodes.get(i).getType().getSuperclass().equals(Number.class)
                    || allChildNodes.get(i).getType().equals(Boolean.class))
                {
                    ctorParameters[i] = allChildNodes.get(i).get(root);
                }
                else{
                    if(allChildNodes.get(i).getName().equalsIgnoreCase("parent") ||
                        allChildNodes.get(i).getName().equalsIgnoreCase("openScope") ||
                        allChildNodes.get(i).getName().equalsIgnoreCase("closeScope"))
                    {
                        ctorParameters[i] = null;
                    }
                    else{
                        // Should line information be copied as well?
                        ctorParameters[i] = DeepCopySubtreeRefl((SyntaxNode) allChildNodes.get(i).get(root));
                    }
                }
            }

            SyntaxNode rootClone;

            if(root instanceof OptimizerUnrolledLoopClosedFor ||
                root instanceof OptimizerUnrolledLoopOpenFor)
            {
                // Get the public ctor. To further improve this, implement search for the ctor with public modifier.
                rootClone = (SyntaxNode) root.getClass().getDeclaredConstructors()[1].newInstance(ctorParameters);
            }
            else{
                rootClone = (SyntaxNode) root.getClass().getDeclaredConstructors()[0].newInstance(ctorParameters);
            }

            for(Object childClone : ctorParameters){
                if(childClone != null && childClone instanceof SyntaxNode){
                    ((SyntaxNode)childClone).setParent(rootClone);
                }
            }

            return rootClone;
        } 
        catch (IllegalArgumentException | IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public static String GetDesignatorName(String originalName, int currentInlineScopeId){
		StringBuilder name = new StringBuilder();
		name.append(originalName);
		
        // Value -1 represents it's not inlined
		if(currentInlineScopeId >= 0){
			name.append("\\i");
			name.append(currentInlineScopeId);
		}

		// Loop unrolling naming scheme

		return name.toString();
	}

    private static Obj FindExpressionSingleObj(Expression expression){
        if(expression instanceof ExpressionSinglePositiveClass){
            Term term = ((ExpressionSinglePositiveClass)expression).getTerm();

            if(term instanceof TermSingleClass){
                Factor factor = ((TermSingleClass)term).getFactor();

                if(factor instanceof FactorDesignatorOnlyClass){
                    return ((DesignatorClass)((FactorDesignatorOnlyClass)factor).getDesignator()).obj;
                }
                else if(factor instanceof FactorConstNumClass){
                    return new Obj(Obj.Con, null, Tab.intType, ((FactorConstNumClass)factor).getC1(), -1);
                }
                else if(factor instanceof FactorConstCharClass){
                    return null;
                }
                else if(factor instanceof FactorConstBoolClass){
                    return null;
                }
            }
        }

        return null;
    }

    private static int CalculateNumberOfIterationsHelper(DesignatorStatement0Or1 initializeFor, Condition0Or1 conditionFor, DesignatorStatement0Or1 updateFor){        
        if(initializeFor instanceof NoDesignatorStatement0Or1Class){
            return -1;
        }

        DesignatorStatement initialize = ((DesignatorStatement0Or1Class)initializeFor).getDesignatorStatement();

        Obj variableInitialize, constantInitialize;

        if(initialize instanceof DesignatorStatementAssignClass){
            variableInitialize = ((DesignatorStatementAssignClass)initialize).getDesignator().obj;
            constantInitialize = FindExpressionSingleObj(((DesignatorStatementAssignClass)initialize).getExpression());
        }
        else{
            // Value cannot be determined if initailize is not similar to <var> AssignOp <con>
            return -1;
        }

        if(variableInitialize != null && constantInitialize != null){
            if(variableInitialize.getKind() != Obj.Var || constantInitialize.getKind() != Obj.Con){
                // Value cannot be determined if initailize is not similar to <var> AssignOp <con>
                return -1;
            }
        }
        else{
            // Value cannot be determined if initailize is not similar to <var> AssignOp <con>
            return -1;
        }
        
        if(conditionFor instanceof NoCondition0Or1Class){
            return -1;
        }

        Condition condition = ((Condition0Or1Class)conditionFor).getCondition();

        if(condition instanceof ConditionRecursiveClass){
            return -1;
        }

        ConditionTerm conditionTerm = ((ConditionSingleClass)condition).getConditionTerm();

        if(conditionTerm instanceof ConditionTermRecursiveClass){
            return -1;
        }

        ConditionFact conditionFact = ((ConditionTermSingleClass)conditionTerm).getConditionFact();

        if(conditionFact instanceof ConditionFactSingleClass){
            return -1;
        }

        Obj variableCondition, constantCondition;
        Obj leftExpressionObj = FindExpressionSingleObj(((ConditionFactRelationalClass)conditionFact).getExpression());
        Obj rightExpressionObj = FindExpressionSingleObj(((ConditionFactRelationalClass)conditionFact).getExpression1());

        if(leftExpressionObj != null && rightExpressionObj != null){
            if(leftExpressionObj.getKind() == Obj.Var && rightExpressionObj.getKind() == Obj.Con){
                variableCondition = leftExpressionObj;
                constantCondition = rightExpressionObj;
            }
            else if(leftExpressionObj.getKind() == Obj.Con && rightExpressionObj.getKind() == Obj.Var){
                variableCondition = rightExpressionObj;
                constantCondition = leftExpressionObj;
            }
            else{
                // Value cannot be determined for conditions not similar to <var> RelOp <con> or <con> RelOp <var>
                return -1;
            }
        }
        else{
            // Value cannot be determined for conditions not similar to <var> RelOp <con> or <con> RelOp <var>
            return -1;
        }

        if(updateFor instanceof NoDesignatorStatement0Or1Class){
            return -1;
        }

        DesignatorStatement update = ((DesignatorStatement0Or1Class)updateFor).getDesignatorStatement();

        Obj variableUpdate;

        if(update instanceof DesignatorStatementIncClass){
            variableUpdate = ((DesignatorStatementIncClass)update).getDesignator().obj;
        }
        else if(update instanceof DesignatorStatementDecClass){
            variableUpdate = ((DesignatorStatementDecClass)update).getDesignator().obj;
        }
        else{
            // Value cannot be determined if update is not similar to <var> INC or <var> DEC
            return -1;
        }

        if(variableUpdate != null){
            if(variableUpdate.getKind() != Obj.Var){
                // Value cannot be determined if update is not similar to <var> INC or <var> DEC
                return -1;
            }
        }
        else{
            // Value cannot be determined if update is not similar to <var> INC or <var> DEC
            return -1;
        }

        // Final checks
        if(variableInitialize != variableCondition || variableCondition != variableUpdate){
            // Value cannot be determined if all three stages don't reference the same variable
            return -1;
        }

        int startValue = constantInitialize.getAdr();
        int endValue = constantCondition.getAdr();
        int updateStep = update instanceof DesignatorStatementIncClass ? 1 : -1;
        int orEqualPlusOne = 0;
        RelOp conditionRelationalOperator = ((ConditionFactRelationalClass)conditionFact).getRelOp();
        boolean startingConditionTrue = false;
    
        if(variableCondition == leftExpressionObj){
            if(conditionRelationalOperator instanceof RelOpGtClass){
                startingConditionTrue = startValue > endValue;
            }
            else if(conditionRelationalOperator instanceof RelOpGteClass){
                startingConditionTrue = startValue >= endValue;
                orEqualPlusOne = 1;
            }
            else if(conditionRelationalOperator instanceof RelOpLtClass){
                startingConditionTrue = startValue < endValue;
            }
            else if(conditionRelationalOperator instanceof RelOpLteClass){
                startingConditionTrue = startValue <= endValue;
                orEqualPlusOne = 1;
            }
            else{
                // Value cannot be determined if relational operator is == or !=
                return -1;
            }
        }
        else{
            // variableCondition == rightExpressionObj
            if(conditionRelationalOperator instanceof RelOpGtClass){
                startingConditionTrue = endValue > startValue;
            }
            else if(conditionRelationalOperator instanceof RelOpGteClass){
                startingConditionTrue = endValue >= startValue;
                orEqualPlusOne = 1;
            }
            else if(conditionRelationalOperator instanceof RelOpLtClass){
                startingConditionTrue = endValue < startValue;
            }
            else if(conditionRelationalOperator instanceof RelOpLteClass){
                startingConditionTrue = endValue <= startValue;
                orEqualPlusOne = 1;
            }
            else{
                // Value cannot be determined if relational operator is == or !=
                return -1;
            }
        }

        if(!startingConditionTrue){
            // If starting condition is not true, number of iterations is 0
            return 0;
        }

        int numberOfIterations = (endValue - startValue + orEqualPlusOne) / updateStep;

        return numberOfIterations >= 0 ? numberOfIterations : -1;
    }

    // Value -1 represents that number of iterations cannot be calculated
    public static int CalculateNumberOfIterations(StatementClosedForClass closedFor){
        return CalculateNumberOfIterationsHelper(closedFor.getDesignatorStatement0Or1(), closedFor.getCondition0Or1(), closedFor.getDesignatorStatement0Or11());
    }

    // TO-DO: Implement proper static code analysis
    public static int CalculateNumberOfIterations(StatementOpenForClass openFor){
        return CalculateNumberOfIterationsHelper(openFor.getDesignatorStatement0Or1(), openFor.getCondition0Or1(), openFor.getDesignatorStatement0Or11());
    }
    
    public static void ReplaceChildRefl(SyntaxNode parent, SyntaxNode oldChild, SyntaxNode newChild){
        try{
            Field[] childNodes = parent.getClass().getDeclaredFields();
        
            for(int i = 0; i < childNodes.length; i++) {
                childNodes[i].setAccessible(true);

                if(childNodes[i].get(parent) == oldChild){
                    childNodes[i].set(parent, newChild);

                    break;
                }
            }

        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

    public static boolean IsInUnrolledFor(SyntaxNode node){
        SyntaxNode parent = node.getParent();

        while(parent != null){
            if((parent instanceof OptimizerUnrolledLoopClosedFor || parent instanceof OptimizerUnrolledLoopOpenFor))
            {
                return true;
            }
            else if(parent instanceof StatementClosedForClass || parent instanceof StatementOpenForClass){
                return false;
            }

            parent = parent.getParent();
        }

        return false;
    }

}