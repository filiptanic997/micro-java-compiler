package rs.ac.bg.etf.pp1.optimizer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.constants.Constants.OptimizationType;
import rs.ac.bg.etf.pp1.optimizer.ast.*;
import rs.ac.bg.etf.pp1.heuristic.Heuristic;
import rs.ac.bg.etf.pp1.util.*;

public class InlineOptimizerPass extends VisitorAdaptor implements IOptimizerPass {
    
    private Map<String, MethodDeclarationClass> methodDeclarationNodes = new HashMap<>();
    //private OptimizationType optimizationType = OptimizationType.GenerateOptimizedCode;
	private int currentInlineLevel = 0;
	private Map<String, Integer> methodCallCounts;
	private Set<String> inlineMethods = new HashSet<>();

    public InlineOptimizerPass(OptimizationType optimizationType, Map<String, Integer> methodCallCounts){
        //this.optimizationType = optimizationType;
        this.methodCallCounts = methodCallCounts;

        // Initialize default Heuristic values if non existant?        
    }

    public InlineOptimizerPass(){
        this(OptimizationType.GenerateNonOptimizedCode, null);
    }

    public int GetCurrentInlineLevel(){
        return currentInlineLevel;
    }

    public void PerformPass(SyntaxNode abstractSyntaxTree){
        abstractSyntaxTree.traverseTopDown(this);
    }

    // Determine whether method should be inlined based on inline keyword and heuristics
	private boolean ShouldMethodBeInlined(String methodName){
		return
            //optimizationType == OptimizationType.GenerateOptimizedCode &&
            Heuristic.ShouldInlineMethodOnCurrentInlineLevel(currentInlineLevel) &&
            (inlineMethods.contains(methodName) ||
			Heuristic.ShouldInlineMethodOnDynamicCallCount(methodName, methodCallCounts.get(methodName)) &&
            Heuristic.ShouldInlineMethodOnCallCountToMemoryFootprintRatio(methodName))
			; 
	}

    public void visit(MethodDeclarationClass methodDeclaration){
        MethodName methodNameObject = methodDeclaration.getMethodName();
		String methodName = null;
		
		if(methodNameObject instanceof MethodNameVoidClass){
			methodName = ((MethodNameVoidClass)methodNameObject).getMethodName();
		}
		else{
			methodName = ((MethodNameNonVoidClass)methodNameObject).getMethodName();
		}

        methodDeclarationNodes.put(methodName, methodDeclaration);

        if(methodDeclaration.getInlineMethod() instanceof InlineMethodClass){
			inlineMethods.add(methodName);
		}
    }

    public void visit(MethodCallClass methodCall){
        DesignatorClass designator = (DesignatorClass)methodCall.getDesignator();
		String methodName = ((DesignatorNameClass)designator.getDesignatorName()).getDesignatorIdentifierName();
        
        if(ShouldMethodBeInlined(methodName)){
            MethodDeclarationClass targetMethodDeclarationNode = methodDeclarationNodes.get(methodName);
            
            // Deep copy statements subtree
            Statement0OrMany statements = (Statement0OrMany) Utilities.DeepCopySubtreeRefl(targetMethodDeclarationNode.getStatement0OrMany());
            OptimizerInlinedMethodCallClass inlinedMethodCall = new OptimizerInlinedMethodCallClass(
                targetMethodDeclarationNode.getFormPars0Or1(),
                methodCall.getActualPars0Or1(),
                targetMethodDeclarationNode.getVarDeclaration0OrMany(),
                statements,
                null,
                null
            );

            statements.setParent(inlinedMethodCall);
            methodCall.getActualPars0Or1().setParent(inlinedMethodCall);
            SyntaxNode parent = methodCall.getParent();
            inlinedMethodCall.setParent(parent);

            if(parent instanceof FactorDesignatorMethodCallClass){
                ((FactorDesignatorMethodCallClass) parent).setMethodCall(inlinedMethodCall);
            }
            else if(parent instanceof DesignatorStatementMethodCallClass){
                ((DesignatorStatementMethodCallClass) parent).setMethodCall(inlinedMethodCall);
            }

            methodCall.setActualPars0Or1(null);
            methodCall.setDesignator(null);

            // It should proceed to traverse the newly added subtree as there might be more inlining
            inlinedMethodCall.traverseTopDown(this);
        }
    }

    public void visit(OptimizerInlinedMethodCallClass inlinedMethodCall){
        // Placeholder
    }

    public void visit(OptimizerInlinedOpenScope optimizerInlinedOpenScope) {
        currentInlineLevel++;
    }

    public void visit(OptimizerInlinedCloseScope optimizerInlinedCloseScope) {
        currentInlineLevel--;
    }

}
