package rs.ac.bg.etf.pp1;

import java.util.*;

import javax.swing.text.html.FormSubmitEvent;

import org.apache.log4j.Logger;

import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.symboltable.concepts.*;
import rs.ac.bg.etf.pp1.constants.Constants;
import rs.ac.bg.etf.pp1.optimizer.ast.*;
import rs.ac.bg.etf.pp1.util.Utilities;

public class SemanticAnalyzer extends VisitorAdaptor {

	int globalVarDeclCount = 0;
	int localVarDeclCount = 0;
	int globalMethodCallCount = 0;
	int globalMethodDeclarationCount = 0;
	int forLoopDeclarationCount = 0;

	boolean errorDetected = false;
	
	Type currentVarType;
	Obj currentMethod;
	boolean currentMethodReturnStatement;
	int currentMethodFormParamsCount = 0;
	boolean mainMethod;
	DesignatorNameClass currentDesignatorName;
	ArrayList<Struct> currentActualPars = new ArrayList();
	int forCounter = 0;	
	int globalVarCounter = 0;

	Stack<Queue<String>> currentInlineLevelParameters = new Stack<>();
	Stack<SyntaxNode> lastReturnStatements = new Stack<>();
	int inlinedScopeIdGenerator = 0;
	Stack<Integer> inlinedScopeIds = new Stack<>();
	
	Logger log = Logger.getLogger(getClass());

	public SemanticAnalyzer(boolean shouldWipeSymbolTable){
		if(shouldWipeSymbolTable){{
			Tab.init();
		}}
	}

	public SemanticAnalyzer(){
		this(true);
	}
	
	public void report_error(String message, SyntaxNode info) {
		errorDetected = true;
		StringBuilder msg = new StringBuilder(message);
		int line = (info == null) ? 0: info.getLine();
		if (line != 0)
			msg.append (" na liniji ").append(line);
		log.error(msg.toString());
	}

	public void report_info(String message, SyntaxNode info) {
		StringBuilder msg = new StringBuilder(message); 
		int line = (info == null) ? 0: info.getLine();
		if (line != 0)
			msg.append (" na liniji ").append(line);
		log.info(msg.toString());
	}
	
	private String ObjToString(Obj node) {
		String kind, name, type, address, level;
		
		if(node.getKind() == Obj.Con) {
			kind = "Con ";
		} else if(node.getKind() == Obj.Var) {
			kind = "Var ";
		} else if(node.getKind() == Obj.Type) {
			kind = "Type ";
		} else if(node.getKind() == Obj.Meth) {
			kind = "Meth ";
		} else if(node.getKind() == Obj.Fld) {
			kind = "Fld ";
		} else if(node.getKind() == Obj.Elem) {
			kind = "Elem ";
		} else {//Prog
			kind = "Prog ";
		}
		
		name = node.getName();
		
		if(node.getType().getKind() == Struct.None) {
			type = ": None " + name;
		} else if(node.getType().getKind() == Struct.Int) {
			type = ": Int " + name;
		} else if(node.getType().getKind() == Struct.Char) {
			type = ": Char " + name;
		} else if(node.getType().getKind() == Struct.Array) {
			if(node.getType().getElemType() == null) {
				type = ": Undefined[] " + name;
			}
			else {
				String subType = null;
				if(node.getType().getElemType().getKind() == Struct.Int) {
					subType = "Int";
				} else if(node.getType().getElemType().getKind() == Struct.Char) {
					subType = "Char";
				} else {// Bool
					subType = "Bool";
				}
				type = ": " + subType + "[] " + name;
			}
		} else if (node.getType().getKind() == Struct.Class){
			type = ": Class " + name;
		} else {// Bool
			type = ": Bool " + name;
		}
		
		address = new Integer(node.getAdr()).toString();
		level = new Integer(node.getLevel()).toString();
		
		return kind + type + ", " + address + " , " + level;
	}
	
	public boolean passed() {
		return !this.errorDetected;
	}
	
	public int getGlobalVarCount() {
		return globalVarCounter;
	}

	public int GetCurrentInlineLevel(){
		return currentInlineLevelParameters.size();
	}

	public int GetCurrentInlineScopeId(){
		if(inlinedScopeIds.size() > 0){
			return inlinedScopeIds.peek();
		}
		
		return -1;
	}
	
	public void visit(ProgramNameClass programName) {
		programName.obj = Tab.insert(Obj.Prog, programName.getProgramName(), Tab.noType);
		Tab.openScope();
	}
	
	public void visit(TypeClass type){
		Obj typeNode = Tab.find(type.getTypeName());
		
		if(typeNode == Tab.noObj) {
			report_error("Nije pronadjen tip " + type.getTypeName() + " u tabeli simbola! ", null);
    		type.struct = Tab.noType;
		}
		else {
			if(Obj.Type == typeNode.getKind()){
    			type.struct = typeNode.getType();
    			this.currentVarType = type;
    		}else{
    			report_error("Greska: Ime " + type.getTypeName() + " ne predstavlja tip!", type);
    			type.struct = Tab.noType;
    		}
		}
	}
	
	// Global const declaration semantic analysis
	public void visit(MoreConstSingleDeclarationClass constVarDecl) {
		Obj nameNode = Tab.find(constVarDecl.getConstVarName());
		
		if(nameNode == Tab.noObj) {
			if(constVarDecl.getConstType().struct.assignableTo(this.currentVarType.struct)) {
				Obj constVarNode = Tab.insert(Obj.Con, constVarDecl.getConstVarName(), this.currentVarType.struct);
				
				if(constVarDecl.getConstType() instanceof ConstTypeNumClass) {
					constVarNode.setAdr(((ConstTypeNumClass) constVarDecl.getConstType()).getC1());
					report_info("Deklarisana number konstanta "+ constVarDecl.getConstVarName(), constVarDecl);
				}
				if(constVarDecl.getConstType() instanceof ConstTypeCharClass) {
					constVarNode.setAdr(((ConstTypeCharClass) constVarDecl.getConstType()).getC1());
					report_info("Deklarisana number konstanta "+ constVarDecl.getConstVarName(), constVarDecl);			
				}
				if(constVarDecl.getConstType() instanceof ConstTypeBoolClass) {
					constVarNode.setAdr(((ConstTypeBoolClass) constVarDecl.getConstType()).getC1().equals("true")? 1 : 0);
					report_info("Deklarisana bool konstanta "+ constVarDecl.getConstVarName(), constVarDecl);
				}
				
			}
			else {
				report_error("Greska: Tip konstante " + constVarDecl.getConstVarName() + " ne odgovara deklarisanom tipu ", constVarDecl);
			}
		}
		else {
			report_error("Greska: Konstanta " + constVarDecl.getConstVarName() + " je vec deklarisana!", constVarDecl);
		}		
		
	}
	
	public void visit(ConstTypeNumClass constTypeNum) {		
		constTypeNum.struct = Tab.intType;
	}
	
	public void visit(ConstTypeCharClass constTypeChar) {		
		constTypeChar.struct = Tab.charType;
	}

	public void visit(ConstTypeBoolClass constTypeBool) {		
		constTypeBool.struct = Tab.boolType;
	}
	
	// Global variable declaration semantic analysis
	public void visit(MoreVarGlobalSingleDeclarationClass moreGlobalVarSingleDeclaration) {
		Obj nameNode = Tab.find(moreGlobalVarSingleDeclaration.getGlobalVarName());
		
		if(nameNode == Tab.noObj) {
			
			if(moreGlobalVarSingleDeclaration.getArrayType() instanceof NoArrayTypeClass) {
				Obj globalVarNode = Tab.insert(Obj.Var, moreGlobalVarSingleDeclaration.getGlobalVarName(), currentVarType.struct);
				report_info("Deklarisana globalna promenljiva "+ moreGlobalVarSingleDeclaration.getGlobalVarName(), moreGlobalVarSingleDeclaration);
			}
			else {
				Obj globalVarNode = Tab.insert(Obj.Var, moreGlobalVarSingleDeclaration.getGlobalVarName(), new Struct(Struct.Array, currentVarType.struct));
				report_info("Deklarisana nizovska globalna promenljiva "+ moreGlobalVarSingleDeclaration.getGlobalVarName(), moreGlobalVarSingleDeclaration);
			}
				
		}
		else {
			report_error("Greska: Globala promenljiva " + moreGlobalVarSingleDeclaration.getGlobalVarName() + " je vec deklarisana!", moreGlobalVarSingleDeclaration);
		}
	}
	
	// Local variable declaration semantic analysis (Required for FactorDesignator)
	public void visit(MoreVarSingleDeclarationClass moreVarSingleDeclaration) {
		String localVariableName = Utilities.GetDesignatorName(moreVarSingleDeclaration.getLocalVarName(), GetCurrentInlineScopeId());
		Obj nameNode = Tab.find(localVariableName);
		// Should renaming be possible?
		
		if(nameNode == Tab.noObj) {
			
			if(moreVarSingleDeclaration.getArrayType() instanceof NoArrayTypeClass) {
				Obj globalVarNode = Tab.insert(Obj.Var, localVariableName, this.currentVarType.struct);
				report_info("Deklarisana lokalna promenljiva "+ localVariableName, moreVarSingleDeclaration);
			}
			else {
				Obj globalVarNode = Tab.insert(Obj.Var, localVariableName, new Struct(Struct.Array, this.currentVarType.struct));
				report_info("Deklarisana nizovska lokalna promenljiva "+ localVariableName, moreVarSingleDeclaration);
			}
				
		}
		else {
			report_error("Greska: lokalna promenljiva " + localVariableName + " je vec deklarisana!", moreVarSingleDeclaration);
		}
	}
	
	// Designator array-type access
	public void visit(FactorExpressionClass factorExpression) {
		if(factorExpression.getExpression().struct == Tab.intType) {
			factorExpression.struct = Tab.intType;
		}
		else {
			factorExpression.struct = Tab.noType;
			report_error("Greska: Clanovi aritmetickog izraza nisu int tipa", factorExpression);
		}
	}
	
	public void visit(FactorNewArrayClass factorNewArray) {
		if(factorNewArray.getExpression().struct == Tab.intType) {
			factorNewArray.struct = new Struct(Struct.Array, factorNewArray.getType().struct);
		}
		else {
			report_error("Greska: velicina niza mora biti int tipa", factorNewArray);
		}
	}
	
	public void visit(FactorNewClass factorNew) {
		factorNew.struct = factorNew.getType().struct;
	}
	
	public void visit(FactorConstBoolClass factorConstBool) {
		factorConstBool.struct = Tab.boolType;
	}
	
	public void visit(FactorConstCharClass factorConstChar) {
		factorConstChar.struct = Tab.charType;
	}
	
	public void visit(FactorConstNumClass factorConstNum) {
		factorConstNum.struct = Tab.intType;
	}
	
	public void visit(FactorDesignatorOnlyClass factorDesignator) {
		String originalDesignatorName = ((DesignatorNameClass)((DesignatorClass)factorDesignator.getDesignator()).getDesignatorName()).getDesignatorIdentifierName();
		String designatorName = Utilities.GetDesignatorName(originalDesignatorName,	GetCurrentInlineScopeId());
		Obj nameNode = Tab.find(designatorName);

		// This codepath should activate when a global variable is referenced in an inlined metohod
		if(nameNode == Tab.noObj){
			nameNode = Tab.findGlobalOnly(originalDesignatorName);
		}
		
		if(nameNode != Tab.noObj) {
			if(nameNode.getType().getKind() == Struct.Array) {
				//if it's array access
				if(((DesignatorClass)factorDesignator.getDesignator()).getDesignatorFollowUp() instanceof DesignatorFollowUpClass) {
					factorDesignator.struct = nameNode.getType().getElemType();
				}
				else {
					factorDesignator.struct = nameNode.getType();
				}
			}
			else {
				factorDesignator.struct = nameNode.getType();
			}
		}
	}
	
	public void visit(FactorDesignatorMethodCallClass factorDesignatorCall) {
		factorDesignatorCall.struct = factorDesignatorCall.getMethodCall().struct;
	}

	public void visit(MethodCallClass methodCall){
		String designatorName = ((DesignatorNameClass)((DesignatorClass)methodCall.getDesignator()).getDesignatorName()).getDesignatorIdentifierName();
		Obj nameNode = Tab.find(designatorName);
		globalMethodCallCount++;
		
		if(nameNode != Tab.noObj) {
			if(nameNode.getKind() == Obj.Meth) {
				//parameter check
				ActualPars0Or1 actualPars = methodCall.getActualPars0Or1();
				methodCall.struct = nameNode.getType();
				if(actualPars instanceof ActualPars0Or1Class && nameNode.getLevel() == currentActualPars.size()) {
					ArrayList<Obj> locals = new ArrayList<Obj>(nameNode.getLocalSymbols());
					for(int i = 0; i < currentActualPars.size(); ++i) {
						if(currentActualPars.get(i) == null || i >= locals.size()) {
							report_info("Upozorenje: Greska u izrazu", methodCall);
						}
						else{
							if(nameNode.getName().equals("len") && currentActualPars.get(i).getKind() == Struct.Array) {
								continue;
							}
							if(!currentActualPars.get(i).assignableTo(locals.get(i).getType())) {
								report_error("Greska: stvarni parametar nije u skladu sa fomralnim", methodCall);
							}
						}
					}
				}
				else if (actualPars instanceof NoActualPars0Or1Class && nameNode.getLevel() == 0 && currentActualPars.size() == 0) {
					// perhaps later?
				}
				else {
					report_error("Greska: pogresan broj stvarnih parametara u pozivu metode " + designatorName, methodCall);
				}
				
				currentActualPars.clear();
			}
			else {
				report_error("Greska: " + designatorName + " nije metoda", methodCall);
			}
		}
	}
	
	public void visit(TermSingleClass termSingle) {
		termSingle.struct = termSingle.getFactor().struct;
	}
	
	public void visit(TermRecursiveClass termRecursive) {
		// potential loophole?
		if(termRecursive.getFactor().struct != null && termRecursive.getFactor().struct.getKind() == Struct.Array) {
			termRecursive.struct = termRecursive.getFactor().struct;
		}
		else if(termRecursive.getTerm().struct == Tab.intType && termRecursive.getFactor().struct == Tab.intType) {
			termRecursive.struct = Tab.intType;
		}
		else {
			termRecursive.struct = Tab.noType;
			report_error("Greska: Clanovi aritmetickog izraza nisu int tipa", termRecursive);
		}
	}
	
	public void visit(ExpressionSinglePositiveClass expressionSingle) {
		expressionSingle.struct = expressionSingle.getTerm().struct;
	}
	
	public void visit(ExpressionSingleNegativeClass expressionSingle) {
		// potential loophole?
		if(expressionSingle.getTerm().struct != null && expressionSingle.getTerm().struct.getKind() == Struct.Array) {
			expressionSingle.struct = expressionSingle.getTerm().struct;
		}
		else if(expressionSingle.getTerm().struct == Tab.intType) {
			expressionSingle.struct = Tab.intType;
		}
		else {
			expressionSingle.struct = Tab.noType;
			report_error("Greska: Clanovi aritmetickog izraza nisu int tipa", expressionSingle);
		}
	}
	
	public void visit(ExpressionRecursiveClass expressionRecursive) {
		// potential loophole?
		if(expressionRecursive.getTerm().struct != null && expressionRecursive.getTerm().struct.getKind() == Struct.Array) {
			expressionRecursive.struct = expressionRecursive.getTerm().struct;
		}
		else if(expressionRecursive.getExpression().struct == Tab.intType && expressionRecursive.getTerm().struct == Tab.intType) {
			expressionRecursive.struct = Tab.intType;
		}
		else {
			expressionRecursive.struct = Tab.noType;
			report_error("Greska: Clanovi aritmetickog izraza nisu int tipa", expressionRecursive);
		}
	}
	
	public void visit(DesignatorClass designator) {
		String originalDesignatorName = ((DesignatorNameClass)designator.getDesignatorName()).getDesignatorIdentifierName();
		String designatorName = Utilities.GetDesignatorName(originalDesignatorName,	GetCurrentInlineScopeId());
		Obj nameNode = Tab.find(designatorName);

		// This codepath should activate when a global variable is referenced in an inlined metohod
		if(nameNode == Tab.noObj){
			nameNode = Tab.findGlobalOnly(originalDesignatorName);
		}
		
		if(nameNode != Tab.noObj) {
			designator.obj = nameNode;
			
			if(designator.getDesignatorFollowUp() instanceof DesignatorFollowUpClass) {
				if(nameNode.getType().getKind() == Struct.Array) {
					DesignatorFollowUpClass toCheck = (DesignatorFollowUpClass) designator.getDesignatorFollowUp();
					if(((Expression) toCheck.getExpression()).struct == Tab.intType) {
						// perhaps later?
						report_info("Koristi se nizovska promenljiva " + ObjToString(designator.obj), designator);
					}
					else {
						report_error("Greska: Izraz za index niza nije int tipa kod promenljive " + originalDesignatorName, designator);
					}
				}
				else {
					report_error("Greska: Promenljiva " + originalDesignatorName + " nije nizovskog tipa", designator);
				}
			}
			else {
				report_info("Koristi se " + ObjToString(designator.obj), designator);
			}
		}
		else {
			designator.obj = nameNode;
			report_error("Greska: Promenljiva " + originalDesignatorName + " nije deklarisana", designator);
		}
	}
	
	// Global functions calls
	public void visit(MethodNameNonVoidClass methodName) {
		Obj nameNode = Tab.find(methodName.getMethodName());
		
		if(nameNode == Tab.noObj) {
			currentMethod = Tab.insert(Obj.Meth, methodName.getMethodName(), ((TypeClass)methodName.getType()).struct);
			
			methodName.obj = currentMethod;
			Tab.openScope();
			currentMethodReturnStatement = false;
			report_info("Deklaracija potpisa metode " + methodName.getMethodName(), methodName);
		}
		else {
			report_error("Greska: metoda " + methodName.getMethodName() + " je vec deklarisana", methodName);
		}
	}
	
	public void visit(MethodNameVoidClass methodName) {
		Obj nameNode = Tab.find(methodName.getMethodName());
		
		if(nameNode == Tab.noObj) {
			currentMethod = Tab.insert(Obj.Meth, methodName.getMethodName(), Tab.noType);
			
			methodName.obj = currentMethod;
			Tab.openScope();
			currentMethodReturnStatement = false;
			report_info("Deklaracija potpisa metode " + methodName.getMethodName(), methodName);
		}
		else {
			report_error("Greska: metoda " + methodName.getMethodName() + " je vec deklarisana", methodName);
		}
	}
	
	public void visit(StatementClosedReturnNonVoidClass statementClosedReturn) {
		//report_info("Return non-void", statementClosedReturn.getParent());
		if(currentMethod != null) {
			currentMethodReturnStatement = true;
			
			if(currentMethod.getType() != Tab.noType) {
				if(statementClosedReturn.getExpression().struct.assignableTo(currentMethod.getType())) {
					// perhaps later?
				}
				else {
					report_error("Greska: return izraz se ne slaze sa povratnim tipom metode " + currentMethod.getName(), statementClosedReturn.getParent());
				}
			}
			else {
				report_error("Greska: void metoda " + currentMethod.getName() + " ne moze da vrati vrednost", statementClosedReturn.getParent());
			}
		}
		else {
			// analyze: is it even possible to pass syntax analysis?
			report_error("Greska: Nadjen return statement van tela metode", statementClosedReturn);
		}

		if(GetCurrentInlineLevel() > 0){
			lastReturnStatements.push(statementClosedReturn);
		}
	}
	
	public void visit(StatementClosedReturnVoidClass statementClosedReturn) {
		//report_info("Return void", statementClosedReturn.getParent());
		if(currentMethod != null) {
			currentMethodReturnStatement = true;
			
			if(currentMethod.getType() == Tab.noType) {
				// perhaps later?
			}
			else {
				report_error("Greska: non-void metoda " + currentMethod.getName() + " mora da vrati vrednost", statementClosedReturn.getParent());
			}
		}
		else {
			// analyze: is it even possible to pass syntax analysis?
			report_error("Greska: Nadjen return statement van tela metode", statementClosedReturn.getParent());
		}

		if(GetCurrentInlineLevel() > 0){
			lastReturnStatements.push(statementClosedReturn);
		}
	}

	public void visit(StatementClosedClass statementClosedClass){
		StatementClosed statementClosed = statementClosedClass.getStatementClosed();

		// If a statement is detected, then a previously encountered return statement is guaranteed
		// not to be the final statement of the method. It must be ensured that last return stack retains
		// integrity and that this is not the parent of the detected return statement
		if(!(statementClosed instanceof StatementDesignatorStatementErrorClass) &&
			!(statementClosed instanceof StatementClosedIfElseClass) &&
			!(statementClosed instanceof StatementClosedForClass) &&
			!(statementClosed instanceof StatementClosedScopedStatementClass) &&
			GetCurrentInlineLevel() > 0 &&
			GetCurrentInlineLevel() == lastReturnStatements.size() &&
			lastReturnStatements.peek() != statementClosed)
		{
			lastReturnStatements.pop();
		}
	}
	
	public void visit(MethodDeclarationClass methodDeclaration) {
		if(currentMethod.getName().equals(Constants.MainMethodName)) {
			mainMethod = true;

			if(methodDeclaration.getInlineMethod() instanceof InlineMethodClass){
				report_error("Greska: main metoda ne moze biti inline", methodDeclaration);
			}
		}
		else{
			globalMethodDeclarationCount++;
		}
		
		if(!(currentMethod.getType() != Tab.noType && currentMethodReturnStatement == false)) {
			currentMethod.setLevel(currentMethodFormParamsCount);
			Tab.chainLocalSymbols(currentMethod);
			Tab.closeScope();
			currentMethodReturnStatement = false;
			currentMethodFormParamsCount = 0;
		}
		else {
			if(methodDeclaration.getMethodName() instanceof MethodNameNonVoidClass) {
				report_error("Greska: non-void metoda " + ((MethodNameNonVoidClass)methodDeclaration.getMethodName()).getMethodName() + " ne sadrzi return statement", methodDeclaration);
			}
		}
		
	}
	
	// Formal parameters usage analysis
	public void visit(FormParsSingleClass formParsSingle) {
		String formalParameterName = Utilities.GetDesignatorName(formParsSingle.getFormParamName(), GetCurrentInlineScopeId());
		Obj nameNode = Tab.find(formalParameterName);
		
		if(nameNode == Tab.noObj) {
			Obj formParamNode;
			String arrMsg = "";
			
			if(formParsSingle.getArrayType() instanceof NoArrayTypeClass) {
				formParamNode = Tab.insert(Obj.Var, formalParameterName, currentVarType.struct);
			}
			else {// array type
				formParamNode = Tab.insert(Obj.Var, formalParameterName, new Struct(Struct.Array, currentVarType.struct));
				arrMsg = "nizovski ";
			}
			
			if(GetCurrentInlineLevel() == 0){
				formParamNode.setFpPos(currentMethodFormParamsCount);
				currentMethodFormParamsCount++;
				currentMethod.setLevel(currentMethodFormParamsCount);
			}
			
			report_info("Deklarisan " + arrMsg + "formalni parametar " + formalParameterName, formParsSingle);
		}
		else {
			report_error("Greska: Formalni parametar " + formalParameterName + " je vec deklarisan", formParsSingle);
		}

		if(GetCurrentInlineLevel() > 0){
			currentInlineLevelParameters.peek().add(formalParameterName);
		}
	}

	public void visit(ActualParsSingleClass actualParsSingle) {
		currentActualPars.add(actualParsSingle.getExpression().struct);

		if(GetCurrentInlineLevel() > 0){
			actualParsSingle.obj = Tab.find(currentInlineLevelParameters.peek().remove());
		}
	}

	public void visit(OptimizerInlinedOpenScope openScope){
		currentInlineLevelParameters.push(new LinkedList<String>());
		inlinedScopeIds.push(inlinedScopeIdGenerator++);
	}

	public void visit(OptimizerInlinedCloseScope closeScope){
		// Case when return statement is the final statement in a method's body
		if(GetCurrentInlineLevel() == lastReturnStatements.size()){
			// Line == -1 carries information that this return statement is the final statement in a method's body
			((StatementClosed) lastReturnStatements.pop()).setLine(-1);
		}

		currentInlineLevelParameters.pop();
		inlinedScopeIds.pop();
	}

	public void visit(OptimizerInlinedMethodCallClass inlinedMethod){
		// Placeholder
	}
	
	public void visit(DesignatorStatementMethodCallClass methodCall) {
		methodCall.struct = methodCall.getMethodCall().struct;
	}
	
	// Statements (required for FOR)
	public void visit(StatementClosedPrintClass closedPrint) {
		if(closedPrint.getExpression().struct == Tab.intType || closedPrint.getExpression().struct == Tab.charType || closedPrint.getExpression().struct == Tab.boolType) {
			//perhaps later?
		}
		else {
			report_error("Greska: parametar print funkcije mora biti int, char ili bul tipa", closedPrint);
		}
	}
	
	public void visit(StatementClosedReadClass closedRead) {
		String originalDesignatorName = ((DesignatorNameClass)((DesignatorClass)closedRead.getDesignator()).getDesignatorName()).getDesignatorIdentifierName();
		String designatorName = Utilities.GetDesignatorName(originalDesignatorName,	GetCurrentInlineScopeId());
		Obj nameNode = Tab.find(designatorName);

		// This codepath should activate when a global variable is referenced in an inlined metohod
		if(nameNode == Tab.noObj){
			nameNode = Tab.findGlobalOnly(originalDesignatorName);
		}
		
		if(nameNode.getKind() == Obj.Var) {
			if(nameNode.getType().getKind() == Struct.Array) {
				if(((DesignatorClass)closedRead.getDesignator()).getDesignatorFollowUp() instanceof DesignatorFollowUpClass) {
					// perhaps later?
				}
				else {
					report_error("Greska: argument READ metode " + originalDesignatorName + " ne moze da bude niz", closedRead);
				}
			}
			else {
				if(nameNode.getType() == Tab.intType || nameNode.getType() == Tab.charType || nameNode.getType() == Tab.boolType) {
					// perhaps later?
				}
				else {
					report_error("Greska: argument READ metode " + originalDesignatorName + " mora biti int, char ili bool tipa", closedRead);
				}
			}
		}
		else {
			report_error("Greska: argument READ metode " + originalDesignatorName + " mora biti promenljiva", closedRead);
		}
	}
	
	public void visit(EndUpdateForClass endUpdateFor) {
		++forCounter;
		forLoopDeclarationCount++;
		
		report_info("Deklarisana for petlja", endUpdateFor.getParent());
	}
	
	public void visit(StatementClosedContinueClass closedContinue) {
		if(forCounter > 0) {
			// perhaps later?
		}
		else {
			report_error("Greska: continue statement moze da se koristi samo u telu for petlje", closedContinue.getParent());
		}
	}
	
	public void visit(StatementClosedBreakClass closedBreak) {
		if(forCounter > 0) {
			// perhaps later?
		}
		else {
			report_error("Greska: break statement moze da se koristi samo u telu for petlje", closedBreak.getParent());
		}
	}
	
	public void visit(ConditionFactSingleClass conditionFactSingle) {
		if(conditionFactSingle.getExpression().struct.getKind() == Struct.Array) {
			conditionFactSingle.struct = conditionFactSingle.getExpression().struct.getElemType();
		}
		else if(conditionFactSingle.getExpression().struct == Tab.boolType) {
			conditionFactSingle.struct = conditionFactSingle.getExpression().struct;
		}
		else {
			report_error("Greska: relacioni izraz mora biti bool tipa", conditionFactSingle);
		}
	}
	
	public void visit(ConditionFactRelationalClass conditionFactRelational) {
		if(conditionFactRelational.getExpression().struct != null &&
			conditionFactRelational.getExpression1().struct != null &&
			(conditionFactRelational.getExpression().struct.getKind() == Struct.Array && conditionFactRelational.getExpression1().struct == Tab.nullType ||
			 conditionFactRelational.getExpression1().struct.getKind() == Struct.Array && conditionFactRelational.getExpression().struct == Tab.nullType)) {
			// perhaps later?
		}
		else if(conditionFactRelational.getExpression().struct != null &&
			conditionFactRelational.getExpression1().struct != null &&
			conditionFactRelational.getExpression().struct.compatibleWith(conditionFactRelational.getExpression1().struct)) {
			
			if(conditionFactRelational.getExpression().struct.getKind() == Struct.Array ||
					conditionFactRelational.getExpression1().struct.getKind() == Struct.Array) {
				
				if(conditionFactRelational.getRelOp() instanceof RelOpEqClass || conditionFactRelational.getRelOp() instanceof RelOpNeqClass) {
					conditionFactRelational.struct = Tab.boolType;
				}
				else {
					report_error("Greska: u relacionim izrazima nizovskih promenljivih mogu da se koriste samo != i ==", conditionFactRelational);
				}
			}
			else{
				conditionFactRelational.struct = Tab.boolType;
			}
			
		}
		else {
			report_error("Greska: operandi relacionih operatora mogu biti samo int tipa", conditionFactRelational);
		}
	}
	
	public void visit(ConditionTermSingleClass conditionTermSingle) {
		if(conditionTermSingle.getConditionFact().struct == Tab.boolType) {
			conditionTermSingle.struct = conditionTermSingle.getConditionFact().struct;
		}
		else {
			// perhaps later?
		}
	}
	
	public void visit(ConditionTermRecursiveClass conditionTermRecursive) {
		if(conditionTermRecursive.getConditionFact().struct == Tab.boolType && conditionTermRecursive.getConditionTerm().struct == Tab.boolType) {
			conditionTermRecursive.struct = Tab.boolType;
		}
		else {
			//report_error("Greska: operandi relacionih operatora mogu biti samo int tipa", conditionTermRecursive);
		}
	}
	
	public void visit(ConditionSingleClass conditionSingle) {
		if(conditionSingle.getConditionTerm().struct == Tab.boolType) {
			conditionSingle.struct = conditionSingle.getConditionTerm().struct;
		}
		else {
			// perhaps later?
		}
	}
	
	public void visit(ConditionRecursiveClass conditionRecursive) {
		if(conditionRecursive.getConditionTerm().struct == Tab.boolType && conditionRecursive.getCondition().struct == Tab.boolType) {
			conditionRecursive.struct = Tab.boolType;
		}
		else {
			//report_error("Greska: operandi relacionih operatora mogu biti samo int tipa", conditionTermRecursive);
		}
	}
	
	public void visit(StatementClosedForClass closedFor) {
		--forCounter;
	}
	
	public void visit(StatementOpenForClass openedFor) {
		--forCounter;
	}
	
	public void visit(DesignatorStatementAssignClass assignStatement) {
		String originalDesignatorName = ((DesignatorNameClass)((DesignatorClass)assignStatement.getDesignator()).getDesignatorName()).getDesignatorIdentifierName();
		String designatorName = Utilities.GetDesignatorName(originalDesignatorName,	GetCurrentInlineScopeId());
		Obj nameNode = Tab.find(designatorName);

		// This codepath should activate when a global variable is referenced in an inlined metohod
		if(nameNode == Tab.noObj){
			nameNode = Tab.findGlobalOnly(originalDesignatorName);
		}
		
		if(nameNode.getKind() == Obj.Var) {
			if(nameNode.getType().getKind() == Struct.Array) {
				if(assignStatement.getExpression().struct == null) {
					report_info("Upozorenje: doslo je do greske u izrazu", assignStatement);
					return;
				}
				if(assignStatement.getExpression().struct.getKind() == Struct.Array) {
					if(assignStatement.getExpression().struct.assignableTo(nameNode.getType())) {
						// perhaps later ?
					}
					else {
						report_error("Greska: ne poklapaju se tipovi operanada dodele", assignStatement);
					}
				}
				else {
					if(assignStatement.getExpression().struct.assignableTo(nameNode.getType().getElemType()) ||
							assignStatement.getExpression().struct == Tab.nullType) {
						// perhaps later ?
					}
					else {
						report_error("Greska: ne poklapaju se tipovi operanada dodele", assignStatement);
					}
				}
			}
			else {
				if(assignStatement.getExpression().struct.assignableTo(nameNode.getType())) {
					// perhaps later ?
				}
				else {
					report_error("Greska: ne poklapaju se tipovi operanada dodele", assignStatement);
				}
			}
		}
		else {
			report_error("Greska: levi operand dodele " + originalDesignatorName + " mora biti promenljiva", assignStatement);
		}
	}
	
	public void visit(DesignatorStatementIncClass incStatement) {
		String originalDesignatorName = ((DesignatorNameClass)((DesignatorClass)incStatement.getDesignator()).getDesignatorName()).getDesignatorIdentifierName();
		String designatorName = Utilities.GetDesignatorName(originalDesignatorName,	GetCurrentInlineScopeId());
		Obj nameNode = Tab.find(designatorName);

		// This codepath should activate when a global variable is referenced in an inlined metohod
		if(nameNode == Tab.noObj){
			nameNode = Tab.findGlobalOnly(originalDesignatorName);
		}
		
		if(nameNode.getKind() == Obj.Var) {
			if(nameNode.getType().getKind() != Struct.Array) {
				if(Tab.intType.assignableTo(nameNode.getType())) {
					// perhaps later ?
				}
				else {
					report_error("Greska: operand inkrementiranja " + originalDesignatorName + " mora da bude int tipa", incStatement);
				}
			}
			else {
				report_error("Greska: operand inkrementiranja " + originalDesignatorName + " ne moze da bude nizovskog tipa", incStatement);
			}
		}
		else {
			report_error("Greska: operand inkrementiranja " + originalDesignatorName + " mora da bude promenljiva", incStatement);
		}
	}
	
	public void visit(DesignatorStatementDecClass decStatement) {
		String originalDesignatorName = ((DesignatorNameClass)((DesignatorClass)decStatement.getDesignator()).getDesignatorName()).getDesignatorIdentifierName();
		String designatorName = Utilities.GetDesignatorName(originalDesignatorName,	GetCurrentInlineScopeId());
		Obj nameNode = Tab.find(designatorName);

		// This codepath should activate when a global variable is referenced in an inlined metohod
		if(nameNode == Tab.noObj){
			nameNode = Tab.findGlobalOnly(originalDesignatorName);
		}
		
		if(nameNode.getKind() == Obj.Var) {
			if(nameNode.getType().getKind() != Struct.Array) {
				if(Tab.intType.assignableTo(nameNode.getType())) {
					// perhaps later ?
				}
				else {
					report_error("Greska: operand dekrementiranja " + originalDesignatorName + " mora da bude int tipa", decStatement);
				}
			}
			else {
				report_error("Greska: operand dekrementiranja " + originalDesignatorName + " ne moze da bude nizovskog tipa", decStatement);
			}
		}
		else {
			report_error("Greska: operand dekrementiranja " + originalDesignatorName + " mora da bude promenljiva", decStatement);
		}
	}
	
	public void visit(ProgramClass program) {
		globalVarCounter = Tab.currentScope.getnVars();
		
		Tab.chainLocalSymbols(program.getProgramName().obj);
		Tab.closeScope();
		
		if(!mainMethod) {
			report_error("Greska: Program mora da sadrzi main metodu", program);
		}
	}
	
}
