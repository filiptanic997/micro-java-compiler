package rs.ac.bg.etf.pp1.optimizer;

import rs.ac.bg.etf.pp1.ast.SyntaxNode;

public interface IOptimizerPass {
    void PerformPass(SyntaxNode abstractSyntaxTree);
}
