package rs.ac.bg.etf.pp1.optimizer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.optimizer.ast.*;
import rs.ac.bg.etf.pp1.constants.Constants.OptimizationType;
import rs.ac.bg.etf.pp1.heuristic.Heuristic;
import rs.ac.bg.etf.pp1.util.Utilities;

public class LoopUnrollingOptimizerPass extends VisitorAdaptor implements IOptimizerPass {

    private enum PassMode {
        IdentifyLoopsToUnroll,
        PerformUnrolling
    }

    private PassMode passMode = PassMode.IdentifyLoopsToUnroll;
    private int loopIdCounter = 0;
    //private OptimizationType optimizationType;
    private List<Integer> loopIterationCounts;
    private Set<SyntaxNode> loopsToUnroll = new HashSet<>();

    public LoopUnrollingOptimizerPass(OptimizationType optimizationType, List<Integer> loopIterationCounts){
        //this.optimizationType = optimizationType;
        this.loopIterationCounts = loopIterationCounts;

        // Initialize default Heuristic values if non existant?        
    }

    public LoopUnrollingOptimizerPass(){
        this(OptimizationType.GenerateNonOptimizedCode, null);
    }

    @Override
    public void PerformPass(SyntaxNode abstractSyntaxTree){
        abstractSyntaxTree.traverseTopDown(this);
        this.passMode = PassMode.PerformUnrolling;
        abstractSyntaxTree.traverseBottomUp(this);
    }

    private boolean ShouldUnrollLoop(StatementClosedForClass closedFor, int loopId){
        return
            //optimizationType == OptimizationType.GenerateOptimizedCode &&
            (IsOptimizedKeywordPresent(closedFor.getOptimizedFor())
            ||
            loopIterationCounts != null && Heuristic.ShouldUnrollLoopOnIterationCount(loopIterationCounts.get(loopId)))
            ;
    }

    private boolean ShouldUnrollLoop(StatementOpenForClass openFor, int loopId){
        return
            //optimizationType == OptimizationType.GenerateOptimizedCode &&
            (IsOptimizedKeywordPresent(openFor.getOptimizedFor())
            ||
            loopIterationCounts != null && Heuristic.ShouldUnrollLoopOnIterationCount(loopIterationCounts.get(loopId)))
            ;
    }

    private boolean IsOptimizedKeywordPresent(OptimizedFor optimizedKeyword){
        return optimizedKeyword instanceof OptimizedForClass;
    }

    public void visit(StatementClosedForClass closedFor){
        if(passMode == PassMode.IdentifyLoopsToUnroll){
            if(ShouldUnrollLoop(closedFor, loopIdCounter++)){
                loopsToUnroll.add(closedFor);
            }
        }
        // PerformUnrolling
        else{
            if(loopsToUnroll.contains(closedFor)){
                int numberOfIterations = Utilities.CalculateNumberOfIterations(closedFor);
                int unrollFactor = Heuristic.UnrollFactor();
                int peelFactor = numberOfIterations % unrollFactor;

                if(numberOfIterations <= 0){
                    return;
                }

                DesignatorStatement0Or1 initialize = closedFor.getDesignatorStatement0Or1();
                OptimizerUnrolledLoopIteration peeledOffIterations = null;

                for(int i = 0; i < peelFactor; i++){
                    StatementClosed statementsClone = (StatementClosed) Utilities.DeepCopySubtreeRefl(closedFor.getStatementClosed());
                    peeledOffIterations = new OptimizerUnrolledLoopIterationClosedStatement(
                        null,
                        peeledOffIterations,
                        new OptimizerUnrolledBeginIteration(),
                        new OptimizerUnrolledEndIteration(),
                        (DesignatorStatement0Or1) Utilities.DeepCopySubtreeRefl(closedFor.getDesignatorStatement0Or11()),
                        statementsClone);
                }

                OptimizerUnrolledLoopIteration unrolledBody = null;

                for(int i = 0; i < unrollFactor; i++){
                    StatementClosed statementsClone = (StatementClosed) Utilities.DeepCopySubtreeRefl(closedFor.getStatementClosed());
                    unrolledBody = new OptimizerUnrolledLoopIterationClosedStatement(
                        null,
                        unrolledBody,
                        new OptimizerUnrolledBeginIteration(),
                        new OptimizerUnrolledEndIteration(),
                        (DesignatorStatement0Or1) Utilities.DeepCopySubtreeRefl(closedFor.getDesignatorStatement0Or11()),
                        statementsClone);
                }

                OptimizerUnrolledLoopClosedFor unrolledLoop = new OptimizerUnrolledLoopClosedFor(
                    new OptimizerUnrolledBeginFor(),
                    initialize,
                    peeledOffIterations,
                    new OptimizerUnrolledBeginCondition(),
                    closedFor.getCondition0Or1(),
                    unrolledBody,
                    new OptimizerUnrolledEndFor());

                unrolledLoop.setParent(closedFor.getParent());
                Utilities.ReplaceChildRefl(closedFor.getParent(), closedFor, unrolledLoop);
            }
        }
    }

    public void visit(StatementOpenForClass openFor){
        if(passMode == PassMode.IdentifyLoopsToUnroll){
            if(ShouldUnrollLoop(openFor, loopIdCounter++)){
                loopsToUnroll.add(openFor);
            }
        }
        // PerformUnrolling
        else{
            if(loopsToUnroll.contains(openFor)){
                int numberOfIterations = Utilities.CalculateNumberOfIterations(openFor);
                int unrollFactor = Heuristic.UnrollFactor();
                int peelFactor = numberOfIterations % unrollFactor;

                DesignatorStatement0Or1 initialize = openFor.getDesignatorStatement0Or1();
                OptimizerUnrolledLoopIteration peeledOffIterations = null;

                for(int i = 0; i < peelFactor; i++){
                    StatementOpen statementsClone = (StatementOpen) Utilities.DeepCopySubtreeRefl(openFor.getStatementOpen());
                    peeledOffIterations = new OptimizerUnrolledLoopIterationOpenStatement(
                        null,
                        peeledOffIterations,
                        new OptimizerUnrolledBeginIteration(),
                        new OptimizerUnrolledEndIteration(),
                        (DesignatorStatement0Or1) Utilities.DeepCopySubtreeRefl(openFor.getDesignatorStatement0Or11()),
                        statementsClone);
                }

                OptimizerUnrolledLoopIteration unrolledBody = null;

                for(int i = 0; i < unrollFactor; i++){
                    StatementOpen statementsClone = (StatementOpen) Utilities.DeepCopySubtreeRefl(openFor.getStatementOpen());
                    unrolledBody = new OptimizerUnrolledLoopIterationOpenStatement(
                        null,
                        unrolledBody,
                        new OptimizerUnrolledBeginIteration(),
                        new OptimizerUnrolledEndIteration(),
                        (DesignatorStatement0Or1) Utilities.DeepCopySubtreeRefl(openFor.getDesignatorStatement0Or11()),
                        statementsClone);
                }

                OptimizerUnrolledLoopClosedFor unrolledLoop = new OptimizerUnrolledLoopClosedFor(
                    new OptimizerUnrolledBeginFor(),
                    initialize,
                    peeledOffIterations,
                    new OptimizerUnrolledBeginCondition(),
                    openFor.getCondition0Or1(),
                    unrolledBody,
                    new OptimizerUnrolledEndFor());

                unrolledLoop.setParent(openFor.getParent());
                Utilities.ReplaceChildRefl(openFor.getParent(), openFor, unrolledLoop);
            }
        }
    }
    
}
