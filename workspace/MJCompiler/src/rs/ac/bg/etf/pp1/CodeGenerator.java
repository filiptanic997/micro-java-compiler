package rs.ac.bg.etf.pp1;

import java.util.ArrayList;
import java.util.Map;
import java.util.Stack;
import java.util.LinkedHashMap;
import java.util.List;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.constants.Constants;
import rs.ac.bg.etf.pp1.constants.Constants.OptimizationType;
import rs.ac.bg.etf.pp1.optimizer.ast.*;
import rs.ac.bg.etf.pp1.util.Utilities;
import rs.etf.pp1.symboltable.concepts.*;
import rs.etf.pp1.mj.runtime.Code;

public class CodeGenerator extends VisitorAdaptor {	
	private int mainPc;
	private Stack<ArrayList<Integer>> thenJumpAddress = new Stack<>();
	private Stack<ArrayList<Integer>> elseJumpAddress = new Stack<>();
	
	private Stack<Integer> beginConditionForStack = new Stack<>();
	private Stack<Integer> beginUpdateForStack = new Stack<>();
	private Stack<ArrayList<Integer>> breakStack = new Stack<>();
	private Stack<ArrayList<Integer>> continueJumpStack = new Stack();

	private OptimizationType optimizationType = OptimizationType.GenerateNonOptimizedCode;
	private int callTableAddress = 0;
	private int methodCallIdCounter = 0;
	private Map<String, Integer> methodCallIds = new LinkedHashMap<>();
	private Map<String, Integer> methodMemoryFootprints = new LinkedHashMap<>();
	private int currentInlineLevel = 0;
	private Stack<String> currentMethodNames = new Stack<>();
	private Stack<List<Integer>> inlinedReturnsForFixup = new Stack<>();
	private int loopTableAddress = 0;
	private int loopIdCounter = 0;

	// Initializes the call table to 0 values
	private void initGlobalFunctionCallCountTable(int callTableSizeInBytes){
		for (int i = 0; i < callTableSizeInBytes; i++) {
			Code.loadConst(0);
			Code.put(Code.putstatic);
			Code.put2(callTableAddress + i);
		}
	}

	private void initLoopIterationCountTable(int loopTableSizeInBytes){
		for (int i = 0; i < loopTableSizeInBytes; i++) {
			Code.loadConst(0);
			Code.put(Code.putstatic);
			Code.put2(loopTableAddress + i);
		}
	}

	private void initGlobalFunctions() {
		Code.put(Code.enter);
        Obj ordMethod = Tab.find("ord");
        ordMethod.setAdr(Code.pc);
        Obj chrMethod = Tab.find("chr");
        chrMethod.setAdr(Code.pc);
        Code.put(1);
        Code.put(1);
        Code.put(Code.load_n);
        Code.put(Code.exit);
        Code.put(Code.return_);
 
        Code.put(Code.enter);
        Obj lenMethod = Tab.find("len");
        lenMethod.setAdr(Code.pc);        
        Code.put(1);
        Code.put(1);
        Code.put(Code.load_n);
        Code.put(Code.arraylength);
        Code.put(Code.exit);
        Code.put(Code.return_);
	}
	
	public CodeGenerator(
		OptimizationType optimizationType,
		int callTableAddress,
		int callTableSizeInBytes,
		int loopTableSizeInBytes)
	{
		this.optimizationType = optimizationType;
		this.callTableAddress = callTableAddress;
		this.loopTableAddress = callTableAddress + callTableSizeInBytes;

		if(optimizationType == OptimizationType.GenerateCallCountCode){
			initGlobalFunctionCallCountTable(callTableSizeInBytes);
			initLoopIterationCountTable(loopTableSizeInBytes);
		}		

		initGlobalFunctions();
	}

	public CodeGenerator(){
		this(OptimizationType.GenerateNonOptimizedCode, 0, 0, 0);
	}
	
	public int getMainPc(){
		return this.mainPc;
	}

	public Map<String, Integer> GetMethodCallIds(){
		return this.methodCallIds;
	}

	public Map<String, Integer> GetMethodMemoryFootprints(){
		return this.methodMemoryFootprints;
	}
	
	private void printBuf(byte[] buf) {
		for(byte c : buf) {
			System.out.println(c);
		}
		//System.out.println(" ");
	}

	public int GetCurrentInlineLevel(){
		return currentInlineLevel;
	}

	private void insertMethodCallCountIncrementCode(int methodIndex){
		Code.put(Code.getstatic);
		Code.put2(callTableAddress + methodIndex);
		Code.loadConst(1);
		Code.put(Code.add);
		Code.put(Code.putstatic);
		Code.put2(callTableAddress + methodIndex);
	}

	private void insertLoopIterationCountIncrementCode(int loopIndex){
		Code.put(Code.getstatic);
		Code.put2(loopTableAddress + loopIndex);
		Code.loadConst(1);
		Code.put(Code.add);
		Code.put(Code.putstatic);
		Code.put2(loopTableAddress + loopIndex);
	}

	// Called after generating code for main method
	private void insertMethodCallCountPrintCode(){
		for (int i = 0; i < methodCallIds.size(); i++) {
			Code.put(Code.getstatic);
			Code.put2(callTableAddress + i);
			Code.loadConst(10);
			Code.put(Code.print);		
		}
	}

	private void insertPrintDelimiterCode(){
		Code.loadConst(110);
		Code.loadConst(10);
		Code.put(Code.bprint);
	}

	// Called after generating code for main method
	private void insertLoopIterationCountPrintCode(){
		for (int i = 0; i < loopIdCounter; i++) {
			Code.put(Code.getstatic);
			Code.put2(loopTableAddress + i);
			Code.loadConst(10);
			Code.put(Code.print);		
		}
	}
	
	public void visit(DesignatorNameClass designatorName) {
		DesignatorClass designator = (DesignatorClass) designatorName.getParent();
		
		if(designator.getParent().getClass().equals(DesignatorStatementAssignClass.class)) {
			if(designator.getDesignatorFollowUp() instanceof DesignatorFollowUpClass) {
				Code.load(designator.obj);
			}
			else {
				// perhaps later?
			}
			
		}
		else {
			if (designator.getParent().getClass().equals(StatementClosedReadClass.class)){
				if(designator.getDesignatorFollowUp() instanceof DesignatorFollowUpClass) {
					Code.load(designator.obj);
				}
			}
			else {//used as rvalue
				if(designator.getDesignatorFollowUp() instanceof DesignatorFollowUpClass) {
					//Code.load(new Obj(Obj.Elem, "$", designator.obj.getType().getElemType()));
					Code.load(designator.obj);
				}
				else {
					Code.load(designator.obj);
				}
			}
		}
	}
	
	public void visit(DesignatorClass designator) {
		
		if(!designator.getParent().getClass().equals(DesignatorStatementAssignClass.class) &&
			!designator.getParent().getClass().equals(FactorDesignatorMethodCallClass.class) &&
			!designator.getParent().getClass().equals(DesignatorStatementMethodCallClass.class) &&
			!designator.getParent().getClass().equals(StatementClosedReadClass.class) &&
			designator.getDesignatorFollowUp() instanceof DesignatorFollowUpClass &&
			designator.obj.getType().getKind() == Struct.Array)
		{
			Code.load(new Obj(Obj.Elem, "$", designator.obj.getType().getElemType()));
		}
		
	}
	
	public void visit(DesignatorStatementIncClass incStatement) {
		Code.loadConst(1);
		Code.put(Code.add);
		Code.store(incStatement.getDesignator().obj);
	}
	
	public void visit(DesignatorStatementDecClass decStatement) {
		Code.loadConst(-1);
		Code.put(Code.add);
		Code.store(decStatement.getDesignator().obj);
	}
	
	public void visit(DesignatorStatementAssignClass designatorStatementAssign) {
		if(((DesignatorClass)designatorStatementAssign.getDesignator()).getDesignatorFollowUp() instanceof DesignatorFollowUpClass) {
			Code.store(new Obj(Obj.Elem, "$", designatorStatementAssign.getDesignator().obj.getType().getElemType()));
		}
		else {
			Code.store(designatorStatementAssign.getDesignator().obj);	
		}
	}
	
	public void visit(MethodNameVoidClass methodNameVoid) {
		
		currentMethodNames.push(methodNameVoid.getMethodName());

		if(Constants.MainMethodName.equals(methodNameVoid.getMethodName())) {
			this.mainPc = Code.pc;
		}
		
		Code.put(Code.enter);
		
		if(!Utilities.IsMethodInlined(methodNameVoid)){
			methodNameVoid.obj.setAdr(Code.pc);
		}

		Code.put(methodNameVoid.obj.getLevel());
		Code.put(methodNameVoid.obj.getLocalSymbols().size());
	}
	
	public void visit(MethodNameNonVoidClass methodNameNonVoid) {
		
		currentMethodNames.push(methodNameNonVoid.getMethodName());

		if(Constants.MainMethodName.equals(methodNameNonVoid.getMethodName())) {
			this.mainPc = Code.pc;
		}
		
		Code.put(Code.enter);
		
		if(!Utilities.IsMethodInlined(methodNameNonVoid)){
			methodNameNonVoid.obj.setAdr(Code.pc);
		}

		Code.put(methodNameNonVoid.obj.getLevel());
		Code.put(methodNameNonVoid.obj.getLocalSymbols().size());
	}
	
	public void visit(MethodDeclarationClass methodDeclaration) {
		String currentMethodName = currentMethodNames.pop();
		MethodName methodNameObject = methodDeclaration.getMethodName();
		String methodName = null;
		
		if(methodNameObject instanceof MethodNameVoidClass){
			methodName = ((MethodNameVoidClass)methodNameObject).getMethodName();
		}
		else{
			methodName = ((MethodNameNonVoidClass)methodNameObject).getMethodName();
		}

		if(optimizationType == OptimizationType.GenerateCallCountCode){
			if(Constants.MainMethodName.equals(methodName)){
				insertMethodCallCountPrintCode();
				insertPrintDelimiterCode();
				insertLoopIterationCountPrintCode();
			}
			else{
				methodCallIds.put(methodName, methodCallIdCounter++);
				methodMemoryFootprints.put(methodName, Utilities.FindCurentMethodMemoryFootprint());
			}
		}

		if(Code.buf[Code.pc - 1] == Code.exit && Utilities.IsMethodInlined(methodDeclaration) ||
			Code.buf[Code.pc - 2] == Code.exit && Code.buf[Code.pc - 1] == Code.return_)
		{
			// Inlined
		}
		else{
			Code.put(Code.exit);

			if(!Utilities.IsMethodInlined(methodDeclaration)){
				Code.put(Code.return_);
			}
		}

	}	

	public void visit(OptimizerInlinedOpenScope optimizerInlinedOpenScope) {
		currentInlineLevel++;

		inlinedReturnsForFixup.push(new ArrayList<Integer>());
	}

	public void visit(OptimizerInlinedCloseScope optimizerInlinedCloseScope) {
		currentInlineLevel--;

		for(int address : inlinedReturnsForFixup.pop()){
			Code.fixup(address);
		}
	}
	
	public void visit(DesignatorStatementMethodCallClass methodCall) {
		// Placeholder
	}
	
	public void visit(FactorDesignatorMethodCallClass factorDesignatorCall) {
		// Placeholder
	}

	public void visit(OptimizerInlinedMethodCallClass factorDesignatorCall){
		// Placeholder
	}

	public void visit(MethodCallClass methodCall){
		DesignatorClass designator = (DesignatorClass)methodCall.getDesignator();
		String methodName = ((DesignatorNameClass)designator.getDesignatorName()).getDesignatorIdentifierName();

		if(optimizationType == OptimizationType.GenerateCallCountCode){
			int methodIndex = methodCallIds.get(methodName);
			insertMethodCallCountIncrementCode(methodIndex);
		}

		Code.put(Code.call);
		Code.put2(methodCall.getDesignator().obj.getAdr() - Code.pc);
	}

	public void visit(ActualParsSingleClass actualParameter){
		if(actualParameter.obj != null){
			Code.store(actualParameter.obj);
		}
	}
	
	public void visit(StatementClosedPrintClass closedPrint) {
		if(this.optimizationType == OptimizationType.GenerateCallCountCode){
			// In case PRINT should be silent, Expression evaluation must be popped from the stack
			Code.put(Code.pop);

			return;
		}

		if(closedPrint.getPrintExpression0Or1() instanceof PrintExpression0Or1Class) {
			Code.loadConst(((PrintExpression0Or1Class) closedPrint.getPrintExpression0Or1()).getNumValue());
		}
		else {
			Code.put(Code.const_5);
		}
		
		if(closedPrint.getExpression().struct == Tab.charType) {
			Code.put(Code.bprint);
		}
		else {
			Code.put(Code.print);
		}
	}
	
	public void visit(StatementClosedReadClass closedRead) {
		if(((DesignatorClass)closedRead.getDesignator()).getDesignatorFollowUp() instanceof DesignatorFollowUpClass) {
			if(closedRead.getDesignator().obj.getType() == Tab.charType) {
				Code.put(Code.bread);
			}
			else {
				Code.put(Code.read);
			}
			
			Code.store(new Obj(Obj.Elem, "$", closedRead.getDesignator().obj.getType().getElemType()));
		}
		else {
			if(closedRead.getDesignator().obj.getType() == Tab.charType) {
				Code.put(Code.bread);
			}
			else {
				Code.put(Code.read);
			}
			
			Code.store(closedRead.getDesignator().obj);
		}
	}
	
	public void visit(StatementClosedReturnNonVoidClass returnNonVoid) {
		if(currentInlineLevel == 0){
			Code.put(Code.exit);
			Code.put(Code.return_);
		}
		else if(returnNonVoid.getLine() != -1){
			Code.putJump(0);
			inlinedReturnsForFixup.peek().add(Code.pc - 2);
		}
	}
	
	public void visit(StatementClosedReturnVoidClass returnVoid) {
		if(currentInlineLevel == 0){
			Code.put(Code.exit);
			Code.put(Code.return_);
		}
		else if(returnVoid.getLine() != -1){
			Code.putJump(0);
			inlinedReturnsForFixup.peek().add(Code.pc - 2);
		}
	}
	
	public void visit(StatementClosedContinueClass closedContinue) {
		if(Utilities.IsInUnrolledFor(closedContinue)){
			Code.putJump(0);
			continueJumpStack.peek().add(Code.pc - 2);
		}
		else{
			Code.putJump(beginUpdateForStack.peek());
		}
	}
	
	public void visit(StatementClosedBreakClass closedBreak) {
		Code.putJump(0);
		breakStack.peek().add(Code.pc - 2);
	}
	
	public void visit(FactorNewArrayClass factorNewArray) {
		Code.put(Code.newarray);
		
		if(factorNewArray.getExpression().struct == Tab.charType) {
			Code.put(0);
		}
		else {
			Code.put(1);
		}
	}
	
	public void visit(FactorConstBoolClass factorConstBool) {
		Obj constBoolNode = Tab.insert(Obj.Con, "$", Tab.boolType);
		constBoolNode.setLevel(0);
		constBoolNode.setAdr(factorConstBool.getC1().equals("true") ? 1 : 0);
		
		Code.load(constBoolNode);
	}
	
	public void visit(FactorConstCharClass factorConstChar) {
		Obj constCharNode = Tab.insert(Obj.Con, "$", Tab.charType);
		constCharNode.setLevel(0);
		constCharNode.setAdr(factorConstChar.getC1());
		
		Code.load(constCharNode);
	}
	
	public void visit(FactorConstNumClass factorConstNum) {
		Obj constNumNode = Tab.insert(Obj.Con, "$", Tab.intType);
		constNumNode.setLevel(0);
		constNumNode.setAdr(factorConstNum.getC1());
		
		Code.load(constNumNode);
	}
	
	public void visit(TermRecursiveClass termRecursive) {
		if(termRecursive.getMulOp() instanceof MulOpMulClass) {
			Code.put(Code.mul);
		}
		else if(termRecursive.getMulOp() instanceof MulOpDivClass) {
			Code.put(Code.div);
		}
		else {// MOD
			Code.put(Code.rem);
		}
	}
	
	public void visit(ExpressionSingleNegativeClass expressionSingleNegative) {
		Code.put(Code.neg);
	}
	
	public void visit(ExpressionRecursiveClass expressionRecursive) {
		if(expressionRecursive.getAddOp() instanceof AddOpAddClass) {
			Code.put(Code.add);
		}
		else {
			Code.put(Code.sub);
		}
	}

	public void visit(BeginConditionIfClass beginConditionIf) {
		thenJumpAddress.push(new ArrayList<Integer>());
		elseJumpAddress.push(new ArrayList<Integer>());
	}
	
	public void visit(ConditionFactSingleClass conditionFactSingle) {
		Code.loadConst(1);
		Code.putFalseJump(Code.eq, 0);
		
		elseJumpAddress.peek().add(Code.pc - 2);
	}
	
	public void visit(ConditionFactRelationalClass conditionFactorRelational) {
		if(conditionFactorRelational.getRelOp() instanceof RelOpEqClass) {
			Code.putFalseJump(Code.eq, Code.pc - 2);
		}
		else if(conditionFactorRelational.getRelOp() instanceof RelOpNeqClass) {
			Code.putFalseJump(Code.ne, Code.pc - 2);
		}
		else if(conditionFactorRelational.getRelOp() instanceof RelOpGtClass) {
			Code.putFalseJump(Code.gt, Code.pc - 2);
		}
		else if(conditionFactorRelational.getRelOp() instanceof RelOpGteClass) {
			Code.putFalseJump(Code.ge, Code.pc - 2);
		}
		else if(conditionFactorRelational.getRelOp() instanceof RelOpLtClass) {
			Code.putFalseJump(Code.lt, Code.pc - 2);
		}
		else {//RelOpLteClass
			Code.putFalseJump(Code.le, Code.pc - 2);
		}
		
		elseJumpAddress.peek().add(Code.pc - 2);
	}
	
	public void visit(LogicalOperatorORClass LogicalOperatorOR) {
		//Code.loadConst(1);// bool: true == 1
		Code.putJump(0);
		thenJumpAddress.peek().add(Code.pc - 2);
		
		for(int addressToFixup : elseJumpAddress.peek()) {
			Code.fixup(addressToFixup);
		}
		
		elseJumpAddress.peek().clear();
	}
	
	public void visit(EndConditionIfClass endConditionIf) {
		for(int addressToFixup : thenJumpAddress.peek()) {
			Code.fixup(addressToFixup);
		}
		
		thenJumpAddress.peek().clear();
	}
	
	public void visit(EndIfClass endIf) {
		//if not, there is also else branch after which the address for the jump on the end of if branch would be fixed
		if(endIf.getParent() instanceof StatementOpenIfClass) {
			thenJumpAddress.pop();
		}
		else {
			Code.putJump(0);
			thenJumpAddress.peek().add(Code.pc - 2);
		}
		
		ArrayList<Integer> previousFactFixups = elseJumpAddress.peek();
		for(int addressToFixup : previousFactFixups) {
			Code.fixup(addressToFixup);
		}
		
		elseJumpAddress.pop();
	}
	
	public void visit(EndElseClass endElse) {
		ArrayList<Integer> previousTermFixups = thenJumpAddress.peek();
		for(int addressToFixup : previousTermFixups) {
			Code.fixup(addressToFixup);
		}
		
		thenJumpAddress.pop();
	}
	
	public void visit(BeginConditionForClass beginConditionFor) {
		beginConditionForStack.push(Code.pc);
		
		thenJumpAddress.push(new ArrayList<Integer>());
		elseJumpAddress.push(new ArrayList<Integer>());
		breakStack.push(new ArrayList<Integer>());
	}
	
	public void visit(BeginUpdateForClass beginUpdateFor) {
		Code.putJump(0);
		thenJumpAddress.peek().add(Code.pc - 2);
		
		beginUpdateForStack.push(Code.pc);
	}
	
	public void visit(EndUpdateForClass endUpdateFor) {
		Code.putJump(beginConditionForStack.pop());
		
		// == beginFor
		ArrayList<Integer> previousTermFixups = thenJumpAddress.pop();
		for(int addressToFixup : previousTermFixups) {
			Code.fixup(addressToFixup);
		}
		
		if(optimizationType == OptimizationType.GenerateCallCountCode){
			insertLoopIterationCountIncrementCode(loopIdCounter++);
		}
	}
	
	public void visit(EndForClass endFor) {
		Code.putJump(beginUpdateForStack.pop());
		
		// resolve elseStack
		ArrayList<Integer> previousFactFixups = elseJumpAddress.pop();
		for(int addressToFixup : previousFactFixups) {
			Code.fixup(addressToFixup);
		}
		
		ArrayList<Integer> previousBreakFixups = breakStack.pop();
		for(int addressToFixup : previousBreakFixups) {
			Code.fixup(addressToFixup);
		}
	}

	public void visit(OptimizerUnrolledBeginFor optimizerUnrolledBeginFor) {
		thenJumpAddress.push(new ArrayList<Integer>());
		elseJumpAddress.push(new ArrayList<Integer>());
		breakStack.push(new ArrayList<Integer>());
	}

	public void visit(OptimizerUnrolledBeginIteration optimizerUnrolledBeginIteration) {
		continueJumpStack.push(new ArrayList<>());
	}

	public void visit(OptimizerUnrolledEndIteration optimizerUnrolledEndIteration) {
		for (Integer continueAddressToFixup : continueJumpStack.pop()) {
			Code.fixup(continueAddressToFixup);
		}
    }

	public void visit(OptimizerUnrolledBeginCondition optimizerUnrolledBeginCondition) {
		beginConditionForStack.push(Code.pc);
    }

    public void visit(OptimizerUnrolledEndFor optimizerUnrolledEndFor) {
		Code.putJump(beginConditionForStack.pop());

		for (Integer breakAddressToFixup : breakStack.pop()) {
			Code.fixup(breakAddressToFixup);
		}

		// resolve elseStack
		ArrayList<Integer> previousFactFixups = elseJumpAddress.pop();
		for(int addressToFixup : previousFactFixups) {
			Code.fixup(addressToFixup);
		}
    }

	public void visit(ProgramClass program) {
		
	}
	
}
