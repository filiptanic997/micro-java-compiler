package rs.ac.bg.etf.pp1;

import org.apache.log4j.Logger;
import rs.ac.bg.etf.pp1.ast.*;

public class RuleVisitor extends VisitorAdaptor{

	int printCallCount = 0;
	int localVarDeclCount = 0;
	int globalVarDeclCount = 0;
	int returnCount = 0;
	
	Logger log = Logger.getLogger(getClass());

	public void visit(MoreVarGlobalSingleDeclarationClass globalvardecl) {
		globalVarDeclCount++;
	}
	
	public void visit(MoreVarSingleDeclarationClass localvardecl){
		localVarDeclCount++;
	}
	
    public void visit(StatementClosedPrintClass print) {
		printCallCount++;
	}

}
