package rs.ac.bg.etf.pp1.constants;

public class Constants {
    public enum OptimizationType {
		GenerateNonOptimizedCode,
		GenerateCallCountCode,
		GenerateOptimizedCode
	}

	public static String MainMethodName = "main";
}
