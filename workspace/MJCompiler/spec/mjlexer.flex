package rs.ac.bg.etf.pp1;

import java_cup.runtime.Symbol;

%%

%{

	// ukljucivanje informacije o poziciji tokena
	private Symbol new_symbol(int type) {
		return new Symbol(type, yyline+1, yycolumn);
	}
	
	// ukljucivanje informacije o poziciji tokena
	private Symbol new_symbol(int type, Object value) {
		return new Symbol(type, yyline+1, yycolumn, value);
	}

%}

%cup
%line
%column

%xstate COMMENT

%eofval{
	return new_symbol(sym.EOF);
%eofval}

%%

/* whitespaces */
" " 	{ }
"\b" 	{ }
"\t" 	{ }
"\r\n" 	{ }
"\f" 	{ }

/* keywords */
"program" 	{ return new_symbol(sym.PROGRAM, yytext()); }
"break"   	{ return new_symbol(sym.BREAK, yytext()); }
"class"   	{ return new_symbol(sym.CLASS, yytext()); }
"abstract"	{ return new_symbol(sym.ABSTRACT, yytext()); }
"else" 		{ return new_symbol(sym.ELSE, yytext()); }
"const" 	{ return new_symbol(sym.CONST, yytext()); }
"if"		{ return new_symbol(sym.IF, yytext()); }
"new" 		{ return new_symbol(sym.NEW, yytext()); }
"print" 	{ return new_symbol(sym.PRINT, yytext()); }
"read" 		{ return new_symbol(sym.READ, yytext()); }
"return" 	{ return new_symbol(sym.RETURN, yytext()); }
"void" 		{ return new_symbol(sym.VOID, yytext()); }
"for" 		{ return new_symbol(sym.FOR, yytext()); }
"extends" 	{ return new_symbol(sym.EXTENDS, yytext()); }
"continue" 	{ return new_symbol(sym.CONTINUE, yytext()); }
"optimized" { return new_symbol(sym.OPTIMIZED, yytext()); }
"inline"   { return new_symbol(sym.INLINE, yytext()); }

/* tokens */
[0-9]+ 				{ return new_symbol(sym.CONST_NUM, new Integer(yytext())); }
"'"."'" 			{ return new_symbol(sym.CONST_CHAR, new Character(yytext().charAt(1))); }
"true"|"false" 		{ return new_symbol(sym.CONST_BOOL, yytext()); }
([a-z]|[A-Z])[a-z|A-Z|0-9|_]* 	{ return new_symbol (sym.IDENTIFIER, yytext()); }

/* operators */
"+" 	{ return new_symbol(sym.ADD, yytext()); }
"-" 	{ return new_symbol(sym.SUB, yytext()); }
"*" 	{ return new_symbol(sym.MUL, yytext()); }
"/" 	{ return new_symbol(sym.DIV, yytext()); }
"%" 	{ return new_symbol(sym.MOD, yytext()); }
"==" 	{ return new_symbol(sym.EQ, yytext()); }
"!=" 	{ return new_symbol(sym.NEQ, yytext()); }
">" 	{ return new_symbol(sym.GT, yytext()); }
">=" 	{ return new_symbol(sym.GTE, yytext()); }
"<" 	{ return new_symbol(sym.LT, yytext()); }
"<=" 	{ return new_symbol(sym.LTE, yytext()); }
"&&" 	{ return new_symbol(sym.AND, yytext()); }
"||" 	{ return new_symbol(sym.OR, yytext()); }
"=" 	{ return new_symbol(sym.ASSIGN, yytext()); }
"++" 	{ return new_symbol(sym.INC, yytext()); }
"--" 	{ return new_symbol(sym.DEC, yytext()); }
";" 	{ return new_symbol(sym.SEMICOLON, yytext()); }
"," 	{ return new_symbol(sym.COMMA, yytext()); }
"." 	{ return new_symbol(sym.POINT, yytext()); }
"(" 	{ return new_symbol(sym.PAR_OPEN, yytext()); }
")" 	{ return new_symbol(sym.PAR_CLOSED, yytext()); }
"[" 	{ return new_symbol(sym.BRACKETS_OPEN, yytext()); }
"]" 	{ return new_symbol(sym.BRACKETS_CLOSED, yytext()); }
"{" 	{ return new_symbol(sym.BRACES_OPEN, yytext()); }
"}" 	{ return new_symbol(sym.BRACES_CLOSED, yytext()); }

<YYINITIAL> "//" { yybegin(COMMENT); }
<COMMENT> .      { yybegin(COMMENT); }
<COMMENT> "\r\n" { yybegin(YYINITIAL); }

. { System.err.println("Leksicka greska ("+yytext()+") u liniji "+(yyline+1)); }






